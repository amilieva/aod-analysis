AOD analysis and columnar processing

## How to run the code -- cmsRun only

cmssw-el7 # FOR CMSSW_14_xxx: run the Singularity and el7 machine, then work with CMSSW_14_xxx; FOR CMSSW_13_xxx: run the next command and CMSSW_13_xxx

cmsrel CMSSW_14_0_14 # CMSSW_13_3_0

cd CMSSW_14_0_14/src/ # CMSSW_13_3_0/src/

cmsenv

git clone https://gitlab.cern.ch/amilieva/aod-analysis.git

scramv1 b

cd aod-analysis/AODAnalysis/AODSkim/test

#FOR DATA:

cmsRun RunGammaGammaLeptonLepton_cfg.py

#FOR MC:

cmsRun RunGammaGammaLeptonLepton_MC_cfg.py 

## How to run the code -- crab

# go to the 'test' directory

../scripts/multicrab.py --create

## Useful CRAB commands

crab submit -c crab_cfg.py

crab status -d multicrab_xxx/yyy # where xxx is a date/time and yyy is the dataset you want to check

