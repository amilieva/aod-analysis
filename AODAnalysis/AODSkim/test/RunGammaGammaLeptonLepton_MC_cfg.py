import FWCore.ParameterSet.Config as cms

from Configuration.StandardSequences.Eras import eras
process = cms.Process('ggll', eras.Run3)
#process = cms.Process('ggll', eras.Run2_2018) 

process.load('Configuration.StandardSequences.Services_cff')
process.load('Configuration.EventContent.EventContent_cff')
process.load('SimGeneral.MixingModule.mixNoPU_cfi')
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')


runOnMC = True
DORERECO = True # Set this to run on-the-fly re-reco of protons with new conditions

#########################
# Direct SIM MC         #
#########################

if runOnMC == True:
        process.load('SimPPS.Configuration.directSimPPS_cff')

process.load("RecoPPS.Configuration.recoCTPPS_cff")

#########################
#    General options    #
#########################

process.load("FWCore.MessageService.MessageLogger_cfi")
process.options   = cms.untracked.PSet(
    #wantSummary = cms.untracked.bool(True),
    #SkipEvent = cms.untracked.vstring('ProductNotFound'),
    allowUnscheduled = cms.untracked.bool(True),
)

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )
process.MessageLogger.cerr.FwkReport.reportEvery = 100

#########################
#          GT           #
#########################

from Configuration.AlCa.GlobalTag import GlobalTag

if runOnMC == False:
        process.GlobalTag = GlobalTag(process.GlobalTag, '140X_dataRun3_Candidate_2024_02_29_14_47_30')
if runOnMC == True:
        process.GlobalTag = GlobalTag(process.GlobalTag, '140X_mcRun3_2024_realistic_v20')

#########################
#      Input files      #
#########################

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(

        # Run 2 2018, Summer 18 dimuon signal MC
	# '/store/mc/RunIISummer20UL18RECO/GGToMuMu_Pt-25_Elastic_13TeV-lpair/AODSIM/106X_upgrade2018_realistic_v11_L1v1-v2/100000/7176B3F5-E427-AF4D-B06E-F86D55A1C378.root'
        # Run 3, Summer22 dimuon signal MC
	'/store/mc/Run3Summer22DRPremix/GGToMuMu_PT-25_El-El_13p6TeV_lpair/AODSIM/124X_mcRun3_2022_realistic_v12-v2/80000/4b641be6-f3ca-4a8c-9425-c73c7f97675f.root'
    ),
    #firstEvent = cms.untracked.uint32(0)
)

#########################
#        Triggers       #
#########################

process.load("AODAnalysis.AODSkim.HLTFilter_cfi")
process.hltFilter.TriggerResultsTag = cms.InputTag("TriggerResults","","HLT")
process.hltFilter.HLTPaths = cms.vstring(
    'HLT_DoubleMu43NoFiltersNoVtx_*',
    'HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8_*',
    'HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass8_*'
)


#########################
#     Proton RECO       #
#########################

if (DORERECO == True) and (runOnMC == False):
    process.ctppsLocalTrackLiteProducer.tagPixelTrack = cms.InputTag("ctppsPixelLocalTracks","","ggll")
    process.ctppsLocalTrackLiteProducer.tagDiamondTrack = cms.InputTag("ctppsDiamondLocalTracks","","ggll")
    process.ctppsProtons.tagLocalTrackLite = cms.InputTag("ctppsLocalTrackLiteProducer","","ggll")
    process.ctppsLocalTrackLiteProducer.includePixels = cms.bool(True)
    process.ctppsLocalTrackLiteProducer.includeDiamonds = cms.bool(True)
    process.ctppsProtons.doSingleRPReconstruction = cms.bool(True)
    process.ctppsProtons.doMultiRPReconstruction = cms.bool(True)

if runOnMC == True:
    process.RandomNumberGeneratorService = cms.Service("RandomNumberGeneratorService",
                                                       beamDivergenceVtxGenerator = cms.PSet(initialSeed = cms.untracked.uint32(3849)),
                                                       ppsDirectProtonSimulation = cms.PSet(initialSeed = cms.untracked.uint32(4981))
                                                       )
    
    from SimPPS.DirectSimProducer.matching_cff import matchDirectSimOutputsAOD
    matchDirectSimOutputsAOD(process)
    
process.load("TrackingTools.TransientTrack.TransientTrackBuilder_cfi")


####################################################################
# Change this part to pick up new Alignment/Optics conditions 
# for re-reconsturcing protons
####################################################################


#########################
#       Analysis        #
#########################

process.load("AODAnalysis.AODSkim.GammaGammaLL_cfi")

process.ggll_aod.triggersList = process.hltFilter.HLTPaths
process.ggll_aod.leptonsType = cms.string('Muon')
process.ggll_aod.runOnMC = cms.bool(runOnMC)
process.ggll_aod.doPUReweighting = cms.bool(False)
process.ggll_aod.fetchProtons = cms.bool(True)
process.ggll_aod.saveExtraTracks = cms.bool(False)

#process.ggll_aod.year = cms.string('2018')
process.ggll_aod.year = cms.string('2023')

if (DORERECO == True) and (runOnMC == False):
    # Use protons re-recoed with new conditions
    process.ggll_aod.ppsRecoProtonSingleRPTag = cms.InputTag("ctppsProtons", "singleRP","ggll")
    process.ggll_aod.ppsRecoProtonMultiRPTag = cms.InputTag("ctppsProtons", "multiRP","ggll")
    process.ggll_aod.ppsLocalTrackTag = cms.InputTag("ctppsLocalTrackLiteProducer","","ggll")

if (DORERECO == False) and (runOnMC == False):
    # Use protons directly from the AOD
    process.ggll_aod.ppsRecoProtonSingleRPTag = cms.InputTag("ctppsProtons", "singleRP")
    process.ggll_aod.ppsRecoProtonMultiRPTag = cms.InputTag("ctppsProtons", "multiRP")

process.ggll_aod.lhcInfoLabel = cms.string('')
process.ggll_aod.stageL1Trigger = cms.uint32(2)

# prepare the output file
process.TFileService = cms.Service('TFileService',
    fileName = cms.string('output_MC.root'),
    closeFileFast = cms.untracked.bool(True)
)

if (DORERECO == True) and (runOnMC == False):
    process.p = cms.Path(
        process.hltFilter*
        process.ctppsDiamondLocalTracks*
        process.ctppsPixelLocalReconstruction *
        process.ctppsLocalTrackLiteProducer *
        process.ctppsProtons * 
        process.ggll_aod
    )
if (DORERECO == False) and (runOnMC == False):
    process.p = cms.Path(
        process.hltFilter*
        process.ggll_aod
    )
if runOnMC == True:
    process.p = cms.Path(
        process.hltFilter *
        process.directSimPPS *
        process.recoDirectSimPPS *
        process.ggll_aod
    )

