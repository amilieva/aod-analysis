from WMCore.Configuration import Configuration
from CRABClient.UserUtilities import config
import os

config = Configuration()

config.section_("General")
config.General.requestName = 'AODAnalysis'
config.General.workArea = dirName #"multicrab_TEST"
config.General.transferOutputs = True
config.General.transferLogs=True

config.section_("JobType")
config.JobType.pluginName = 'Analysis'
config.JobType.psetName = 'RunGammaGammaLeptonLepton_cfg.py'
config.JobType.outputFiles = ['output.root']

config.section_("Data")
config.Data.inputDataset = 'FromMulticrab'#'/Muon1/Run2023D-PromptReco-v1/AOD'
config.Data.inputDBS = 'global'
config.Data.splitting = 'Automatic'#'LumiBased'
config.Data.outLFNDirBase = '/store/user/%s/TEST' % (os.environ.get('USER'))

#config.Data.lumiMask = '../data/PPS-July2023_track_only_AND_CMS_Golden.json'
config.Data.outputDatasetTag = 'AODAnalysis'
config.Data.publication = False

config.section_("Debug")
config.Debug.extraJDL = ['+CMS_ALLOW_OVERFLOW=False']

config.section_("Site")
config.Site.storageSite = "T2_FI_HIP"



