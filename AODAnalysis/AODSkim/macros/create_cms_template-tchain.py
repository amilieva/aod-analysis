import ROOT
import os

def luminosity_text(canvas, luminosity, energy, writeExtraText):
    canvas.cd()

    luminosity += energy

    cmsText     = "CMS"
    cmsTextFont   = 61

    extraTextFont = 52

    lumiTextSize     = 0.6
    lumiTextOffset   = 0.2

    cmsTextSize      = 0.75
    cmsTextOffset    = 0.1
    extraOverCmsTextSize  = 0.76

    relPosX    = 0.045
    relPosY    = 0.035
    relExtraDY = 1.2

    iPos = 10  # 10 - top-left, 11 - top-right, 33 - bottom-left, 31 - bottom-right
    if iPos == 0: 
        relPosX = 0.12

    outOfFrame = False
    if iPos // 10 == 0: 
        outOfFrame = True

    alignY_ = 3
    alignX_ = 2
    if iPos // 10 == 0: 
        alignX_ = 1
    if iPos == 0: 
        alignY_ = 1
    if iPos // 10 == 1: 
        alignX_ = 1
    if iPos // 10 == 2: 
        alignX_ = 2
    if iPos // 10 == 3: 
        alignX_ = 3
    align_ = 10 * alignX_ + alignY_

    H = canvas.GetWh()
    W = canvas.GetWw()
    l = canvas.GetLeftMargin()
    t = canvas.GetTopMargin()
    r = canvas.GetRightMargin()
    b = canvas.GetBottomMargin()
    e = 0.025

    latex = ROOT.TLatex()
    latex.SetNDC(True)
    latex.SetTextAngle(0)
    latex.SetTextColor(ROOT.kBlack)

    extraTextSize = extraOverCmsTextSize * cmsTextSize

    latex.SetTextFont(42)
    latex.SetTextAlign(31)
    latex.SetTextSize(lumiTextSize * t)

    latex.DrawLatex(1 - r, 1 - t + lumiTextOffset * t, luminosity)

    if outOfFrame:
        latex.SetTextFont(cmsTextFont)
        latex.SetTextAlign(11)
        latex.SetTextSize(cmsTextSize * t)
        latex.DrawLatex(l, 1 - t + lumiTextOffset * t, cmsText)

    posX_ = 0
    if iPos % 10 <= 1:
        posX_ = l + relPosX * (1 - l - r)
    elif iPos % 10 == 2:
        posX_ = l + 0.5 * (1 - l - r)
    elif iPos % 10 == 3:
        posX_ = 1 - r - relPosX * (1 - l - r)

    posY_ = 1 - t - relPosY * (1 - t - b)

    if not outOfFrame:
        latex.SetTextFont(cmsTextFont)
        latex.SetTextSize(cmsTextSize * t)
        latex.SetTextAlign(align_)
        latex.DrawLatex(posX_, posY_, cmsText)
        if writeExtraText:
            latex.SetTextFont(extraTextFont)
            latex.SetTextAlign(align_)
            latex.SetTextSize(extraTextSize * t)
            latex.DrawLatex(posX_, posY_ - relExtraDY * cmsTextSize * t, writeExtraText)
    elif writeExtraText:
        if iPos == 0:
            posX_ = l + relPosX * (1 - l - r)
            posY_ = 1 - t + lumiTextOffset * t

        latex.SetTextFont(extraTextFont)
        latex.SetTextSize(extraTextSize * t)
        latex.SetTextAlign(align_)
        latex.DrawLatex(posX_, posY_, writeExtraText)

    canvas.Update()


def cms_template(file_path, luminosity, energy, additional_text, output_file):
    file_name = os.path.basename(file_path).split('.')[0]
    output_directory = f"plots/plots_{file_name}-test"
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)

    W = 800
    H = 600

    canvas = ROOT.TCanvas("canvas", "canvas", 50, 50, W, H)

    # references for T, B, L, R
    T = 0.08 * H
    B = 0.12 * H
    L = 0.12 * W
    R = 0.04 * W

    canvas.SetFillColor(0)
    canvas.SetBorderMode(0)
    canvas.SetFrameFillStyle(0)
    canvas.SetFrameBorderMode(0)
    canvas.SetLeftMargin(L / W)
    canvas.SetRightMargin(R / W)
    canvas.SetTopMargin(T / H)
    canvas.SetBottomMargin(B / H)
    canvas.SetTickx(0)
    canvas.SetTicky(0)

    # Create a TChain and add the file
    chain = ROOT.TChain("ntp1")  # replace "myTree" with your tree name
    chain.Add(file_path)

    hist_list = []

    for branch in chain.GetListOfBranches():
        branch_name = branch.GetName()
        hist_name = f"{branch_name}_hist"
        if "VertexDistXYZ" in branch_name:
            hist = ROOT.TH1F(hist_name, hist_name, 50, 0, 0.05)
        # elif "ExtraTrack_pt" or "ExtraTrack_px" or "ExtraTrack_py" or "ExtraTrack_pz" in branch_name:
        elif "ExtraTrack" in branch_name:
            hist = ROOT.TH1F(hist_name, hist_name, 100, 0, 0.5)
        else:
            hist = ROOT.TH1F(hist_name, hist_name, 50, 0, 200)  # example binning, adjust as needed
        chain.Project(hist_name, branch_name)
        hist_list.append(hist)

    for hist in hist_list:
        ROOT.gStyle.SetOptStat(0)
        hist.SetTitle("")

        if "TH1" in hist.ClassName():
            hist.Draw()
        elif "TH2" in hist.ClassName():
            hist.Draw("colz")

        if additional_text:
            luminosity_text(canvas, luminosity, energy, additional_text)

        output_file_name = os.path.join(output_directory, f"{output_file}_{hist.GetName()}.png")
        canvas.Update()

        print(f"Saving plot to {output_file_name}")
        canvas.SaveAs(output_file_name)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Creates the plots from given ROOT file")
    parser.add_argument("ROOTfile", type=str, help="Path to the actual ROOT file or a pattern for TChain")
    args = parser.parse_args()

    ROOT.gROOT.SetBatch(True)  # Suppress graphical output from ROOT

    print(f"Processing ROOT file: {args.ROOTfile}")
    cms_template(args.ROOTfile, "29.35 fb^{-1}", " (13.6 TeV)", additional_text="Preliminary", output_file="output")
