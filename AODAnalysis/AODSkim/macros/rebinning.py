import ROOT
from datetime import datetime
import argparse
import os

    # fit = ROOT.TF1("fit", "gaus", -0.2, 0.2)
    # fit = ROOT.TF1("fit", "gaus", -5.0, 1.0)


def chi2_distribution(x, par):

    x_val = x[0]
    k = par[0]
    theta = par[1]
    N = par[2]

    if x_val < 0:
        return 0

    return N * (x_val / theta)**(k / 2 - 1) * ROOT.TMath.Exp(-x_val / (2 * theta)) / (2**(k / 2) * ROOT.TMath.Gamma(k / 2))

def lognormal_distribution(x, par):
    x_val = x[0]
    k = par[0]
    theta = par[1]
    N = par[2]

    if x_val < 0:
        return 0

    return N * (1 / (x_val * theta * ROOT.TMath.Sqrt(2 * ROOT.TMath.Pi()))) * ROOT.TMath.Exp(-((ROOT.TMath.Log(x_val) - k)**2) / (2 * theta**2))

def fit(hist, hist_name):
    # print(f"Started to fit {hist_name} with a Chi-Squared function")

    # chi2_func = ROOT.TF1("chi2_func", chi2_distribution, hist.GetXaxis().GetXmin(), hist.GetXaxis().GetXmax(), 3)
    # chi2_func.SetParameters(1, 1, 1)  # k, theta, N
    # chi2_func.SetParNames("k", "theta", "N")
    
    # fit_result = hist.Fit("chi2_func", "RS")

    # fit_parameters = chi2_func.GetParameters()
    # fit_parameters_error = chi2_func.GetParErrors()

    # k = fit_parameters[0]
    # k_error = fit_parameters_error[0]
    # theta = fit_parameters[1]
    # theta_error = fit_parameters_error[1]
    # N = fit_parameters[2]
    # N_error = fit_parameters_error[2]

    # print(f"Fit result: k = {k} ± {k_error}, theta = {theta} ± {theta_error}, N = {N} ± {N_error}")

    # chi_squared = fit_result.Chi2()
    # ndf = fit_result.Ndf()  # Number of degrees of freedom
    # print(f"Chi-Squared: {chi_squared}, NDF: {ndf}, Chi-Squared/NDF: {chi_squared / ndf if ndf != 0 else float('inf')}")

    print(f"Started to fit {hist_name} with a Log-Normal function")

    lognormal_func = ROOT.TF1("lognormal_func", lognormal_distribution, hist.GetXaxis().GetXmin(), hist.GetXaxis().GetXmax(), 3)
    lognormal_func.SetParameters(1, 1, 1)  # k, theta, N
    lognormal_func.SetParNames("k", "theta", "N")

    fit_result = hist.Fit("lognormal_func", "RS")

    fit_parameters = lognormal_func.GetParameters()
    fit_parameters_error = lognormal_func.GetParErrors()
    
    k = fit_parameters[0]
    k_error = fit_parameters_error[0]
    theta = fit_parameters[1]
    theta_error = fit_parameters_error[1]
    N = fit_parameters[2]
    N_error = fit_parameters_error[2]

    print(f"Fit result: k = {k} ± {k_error}, theta = {theta} ± {theta_error}, N = {N} ± {N_error}")

    return fit_result

new_bin_size = 4  # how many bins to merge into one

parser = argparse.ArgumentParser(description="Rebin the histograms from given ROOT file")
parser.add_argument("ROOTfile", type=str, help="Path to the actual ROOT file")
args = parser.parse_args()

ROOT.gROOT.SetBatch(True)  # Suppress graphical output from ROOT

date_now = datetime.now().strftime("%Y-%m-%d")

if not args.ROOTfile.endswith(".root"):
    print("Error: The input file must be a ROOT file")
    exit(1)

file_name = args.ROOTfile

root_file = ROOT.TFile.Open(file_name)
if not root_file:
    print(f"Error: Failed to open ROOT file: {file_name}")
    exit(1)

print(f"Opening file {file_name}")

if "multi" in file_name:
    histogram_names = ["hres45mult1", "hres56mult1", "hressummult"] 
else:
    histogram_names = ["hres451", "hres561", "hressum"]

file_name = os.path.splitext(os.path.basename(file_name))[0] # file name without extension

# create the output directory structure
# rebinned/2024-04-16/file_name/histogram_name
print(f"Creating output directory structure")
output_parent_dir = os.path.join("rebinned", date_now)
output_parent_dir = os.path.join(output_parent_dir, file_name)
os.makedirs(output_parent_dir, exist_ok=True)

print(f"Starting rebinning process")
for histogram_name in histogram_names:
    hist = root_file.Get(histogram_name)
    print(f"hist is {hist}")

    if not hist:
        print(f"Error: Histogram '{histogram_name}' not found in file.")
        continue
    
    if not isinstance(hist, (ROOT.TH1, ROOT.TH1F)):
        print(f"Error: Histogram '{histogram_name}' has incorrect type: {type(hist)}")
        continue

    if new_bin_size < 1:
        print("Error: New bin size must be greater than 0")
        continue    
    
    if hist.GetNbinsX() % new_bin_size != 0:
        print("Error: New bin size must be a divisor of the number of bins in the histogram")
        continue

    rebinned_nbins = hist.GetNbinsX() // new_bin_size
    if hist.GetNbinsX() % new_bin_size != 0:
        rebinned_nbins += 1

    print(f"Original number of bins: {hist.GetNbinsX()}")
    print(f"New number of bins: {rebinned_nbins}")

    new_hist = ROOT.TH1F(f"{histogram_name}_rebinned", f"{histogram_name}_rebinned", rebinned_nbins, hist.GetXaxis().GetXmin(), hist.GetXaxis().GetXmax())
    new_hist.SetTitle(hist.GetTitle())
    new_hist.SetXTitle(hist.GetXaxis().GetTitle())
    new_hist.SetYTitle(hist.GetYaxis().GetTitle())

    print(f"Rebinning histogram {histogram_name} with new bin size {new_bin_size}")

    for i in range(1, hist.GetNbinsX() + 1, new_bin_size):
        bin_content = 0
        bin_error = 0
        for j in range(new_bin_size):
            bin_content += hist.GetBinContent(i + j)
            bin_error += hist.GetBinError(i + j) ** 2

        print(f"Bin {i // new_bin_size}: content = {bin_content}, error = {bin_error ** 0.5}")
        new_hist.SetBinContent(i // new_bin_size, bin_content)
        new_hist.SetBinError(i // new_bin_size, bin_error ** 0.5)

    if new_hist:
        print(f"{histogram_name} rebinning completed. Saving to {histogram_name}_rebinned.root")

        histogram_dir = os.path.join(output_parent_dir, histogram_name)
        os.makedirs(histogram_dir, exist_ok=True)
        output_file_name = f"{date_now}_{file_name}_{histogram_name}_rebinned_newbinsize{new_bin_size}.root"
        output_file_path = os.path.join(histogram_dir, output_file_name)
        output_file = ROOT.TFile(output_file_path, "RECREATE")
        new_hist.Write()

        fit(new_hist, histogram_name)
        new_hist.Write()

        # save as png
        W = 800
        H = 600
        canvas = ROOT.TCanvas("canvas", "canvas", 50, 50, W, H)
        canvas.SetFillColor(0)
        canvas.SetBorderMode(0)
        canvas.SetFrameFillStyle(0)
        canvas.SetFrameBorderMode(0)
        canvas.SetLeftMargin(0.12)
        canvas.SetRightMargin(0.04)
        canvas.SetTopMargin(0.08)

        print(f"Saving plot to {os.path.join(histogram_dir, output_file_name)}\n")
        new_hist.Draw()
        canvas.Update()

        output_png_name = f"{date_now}_{file_name}_{histogram_name}_rebinned_newbinsize{new_bin_size}_fitted.png"

        canvas.SaveAs(os.path.join(histogram_dir, output_png_name))

        output_file.Close()
    else:
        print(f"Failed to rebin histogram {histogram_name}")

root_file.Close()
