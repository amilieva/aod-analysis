import ROOT
import numpy as np

file = ROOT.TFile("../test/multicrab_RunGammaGammaLeptonLepton_cfg_v1330_Run2023BCD_20240227T0947/merged_output.root")

chain = ROOT.TChain("ntp1")
chain.Add("../test/multicrab_RunGammaGammaLeptonLepton_cfg_v1330_Run2023BCD_20240227T0947/merged_output.root")

#chain.Add("merged_output.root")

#setbranchad
Run = np.zeros(1, dtype=int)
LumiSection = np.zeros(1, dtype=int)
EventNum = np.zeros(1, dtype=int)

MuonCand_pt = np.zeros(2, dtype=float)
MuonCand_eta = np.zeros(2, dtype=float)
MuonCand_phi = np.zeros(2, dtype=float)
MuonCand_e = np.zeros(2, dtype=float)
MuonCand_charge = np.zeros(2, dtype=int)
MuonCand_istight = np.zeros(2, dtype=int)

nPair = np.zeros(1, dtype=int)
Pair_mass = np.zeros(1, dtype=float)
Pair_pt = np.zeros(1, dtype=float)
Pair_eta = np.zeros(1, dtype=float)
Pair_phi = np.zeros(1, dtype=float)
Pair_dpt = np.zeros(1, dtype=float)
Pair_dphi = np.zeros(1, dtype=float)

nRecoProtCand = np.zeros(1, dtype=int)
ProtCand_xi = np.zeros(23, dtype=float)
ProtCand_arm = np.zeros(23, dtype=int)
ProtCand_ismultirp = np.zeros(23, dtype=int)
ProtCand_rpid1 = np.zeros(23, dtype=int)

chain.SetBranchAddress("Run", Run)
chain.SetBranchAddress("LumiSection", LumiSection)
chain.SetBranchAddress("EventNum", EventNum)

chain.SetBranchAddress("MuonCand_pt", MuonCand_pt)
chain.SetBranchAddress("MuonCand_eta", MuonCand_eta)
chain.SetBranchAddress("MuonCand_phi", MuonCand_phi)
chain.SetBranchAddress("MuonCand_e", MuonCand_e)
chain.SetBranchAddress("MuonCand_charge", MuonCand_charge)

chain.SetBranchAddress("nPair", nPair)
chain.SetBranchAddress("Pair_mass", Pair_mass)
chain.SetBranchAddress("Pair_pt", Pair_pt)
chain.SetBranchAddress("Pair_eta", Pair_eta)
chain.SetBranchAddress("Pair_phi", Pair_phi)
chain.SetBranchAddress("Pair_dpt", Pair_dpt)
chain.SetBranchAddress("Pair_dphi", Pair_dphi)

chain.SetBranchAddress("nRecoProtCand", nRecoProtCand)
chain.SetBranchAddress("ProtCand_xi", ProtCand_xi)
chain.SetBranchAddress("ProtCand_arm", ProtCand_arm)
chain.SetBranchAddress("ProtCand_ismultirp", ProtCand_ismultirp)
chain.SetBranchAddress("ProtCand_rpid1", ProtCand_rpid1)

# fill histograms for pt, eta, phi, mass, dpt, dphi

h_muon_pt = ROOT.TH1F("h_muon_pt", "Muon p_{T}", 100, 0, 100)
h_muon_eta = ROOT.TH1F("h_muon_eta", "Muon #eta", 100, -3, 3)
h_muon_phi = ROOT.TH1F("h_muon_phi", "Muon #phi", 100, -3.15, 3.15)
h_muon_e = ROOT.TH1F("h_muon_e", "Muon E", 100, 0, 100)
h_muon_charge = ROOT.TH1F("h_muon_charge", "Muon charge", 3, -1, 2)
h_muon_istight = ROOT.TH1F("h_muon_istight", "Muon isTight", 3, -1, 2)

h_pair_mass = ROOT.TH1F("h_pair_mass", "DiMuon mass", 100, 0, 100)
h_pair_pt = ROOT.TH1F("h_pair_pt", "DiMuon p_{T}", 100, 0, 100)
h_pair_eta = ROOT.TH1F("h_pair_eta", "DiMuon #eta", 100, -3, 3)
h_pair_phi = ROOT.TH1F("h_pair_phi", "DiMuon #phi", 100, -3.15, 3.15)
h_pair_dpt = ROOT.TH1F("h_pair_dpt", "DiMuon #Delta p_{T}", 100, 0, 100)
h_pair_dphi = ROOT.TH1F("h_pair_dphi", "DiMuon #Delta #phi", 100, 0, 3.15)

h_prot_xi = ROOT.TH1F("h_prot_xi", "Proton #xi", 100, 0, 0.2)
h_prot_arm = ROOT.TH1F("h_prot_arm", "Proton arm", 3, 0, 3)
h_prot_ismultirp = ROOT.TH1F("h_prot_ismultirp", "Proton isMultirp", 3, -1, 2)
h_prot_rpid1 = ROOT.TH1F("h_prot_rpid1", "Proton RPId1", 25, 0, 25)

h_mumu_acop_mass = ROOT.TH2F("h_mumu_acop_mass", "DiMuon mass vs acoplanarity", 1000, 0, 1000, 1000, 0, 0.01)
h_mumu_acop = ROOT.TH1F("h_mumu_acop", "DiMuon acoplanarity", 100, 0, 0.01)

h_muon_pt.SetTitle("Muon p_{T}")
h_muon_pt.GetXaxis().SetTitle("p_{T} [GeV]")
h_muon_pt.GetYaxis().SetTitle("Entries")

h_muon_eta.SetTitle("Muon #eta")
h_muon_eta.GetXaxis().SetTitle("#eta")
h_muon_eta.GetYaxis().SetTitle("Entries")

h_muon_phi.SetTitle("Muon #phi")
h_muon_phi.GetXaxis().SetTitle("#phi")
h_muon_phi.GetYaxis().SetTitle("Entries")

h_muon_e.SetTitle("Muon E")
h_muon_e.GetXaxis().SetTitle("E [GeV]")
h_muon_e.GetYaxis().SetTitle("Entries")

h_muon_charge.SetTitle("Muon charge")
h_muon_charge.GetXaxis().SetTitle("Charge")
h_muon_charge.GetYaxis().SetTitle("Entries")

h_muon_istight.SetTitle("Muon isTight")
h_muon_istight.GetXaxis().SetTitle("isTight")
h_muon_istight.GetYaxis().SetTitle("Entries")

h_pair_mass.SetTitle("DiMuon mass")
h_pair_mass.GetXaxis().SetTitle("Mass [GeV]")
h_pair_mass.GetYaxis().SetTitle("Entries")

h_pair_pt.SetTitle("DiMuon p_{T}")
h_pair_pt.GetXaxis().SetTitle("p_{T} [GeV]")
h_pair_pt.GetYaxis().SetTitle("Entries")

h_pair_eta.SetTitle("DiMuon #eta")
h_pair_eta.GetXaxis().SetTitle("#eta")
h_pair_eta.GetYaxis().SetTitle("Entries")

h_pair_phi.SetTitle("DiMuon #phi")
h_pair_phi.GetXaxis().SetTitle("#phi")
h_pair_phi.GetYaxis().SetTitle("Entries")

h_pair_dpt.SetTitle("DiMuon #Delta p_{T}")
h_pair_dpt.GetXaxis().SetTitle("#Delta p_{T} [GeV]")
h_pair_dpt.GetYaxis().SetTitle("Entries")

h_pair_dphi.SetTitle("DiMuon #Delta #phi")
h_pair_dphi.GetXaxis().SetTitle("#Delta #phi")
h_pair_dphi.GetYaxis().SetTitle("Entries")

h_prot_xi.SetTitle("Proton #xi")
h_prot_xi.GetXaxis().SetTitle("#xi")
h_prot_xi.GetYaxis().SetTitle("Entries")

h_prot_arm.SetTitle("Proton arm")
h_prot_arm.GetXaxis().SetTitle("Arm")
h_prot_arm.GetYaxis().SetTitle("Entries")

h_prot_ismultirp.SetTitle("Proton isMultirp")
h_prot_ismultirp.GetXaxis().SetTitle("isMultirp")
h_prot_ismultirp.GetYaxis().SetTitle("Entries")

h_prot_rpid1.SetTitle("Proton RPId1")
h_prot_rpid1.GetXaxis().SetTitle("RPId1")
h_prot_rpid1.GetYaxis().SetTitle("Entries")

h_mumu_acop.SetTitle("DiMuon acoplanarity")
h_mumu_acop.GetXaxis().SetTitle("Acoplanarity")
h_mumu_acop.GetYaxis().SetTitle("Entries")

h_mumu_acop_mass.SetTitle("DiMuon mass vs acoplanarity")
h_mumu_acop_mass.GetXaxis().SetTitle("Mass [GeV]")
h_mumu_acop_mass.GetYaxis().SetTitle("Acoplanarity")

# mu1 = ROOT.TLorentzVector()
# mu2 = ROOT.TLorentzVector()
# pair = ROOT.TLorentzVector()

for i in range(chain.GetEntries()):
    chain.GetEntry(i)
    
    #print(f"nRecoProtCand[0] = {nRecoProtCand[0]}")
    
    
    if i % 10000 == 0: print(f"Processing entry: {i} / {chain.GetEntries()}")

    if Pair_mass[0] < 110: continue
    if MuonCand_pt[0] < 20 or MuonCand_pt[1] < 20: continue
    if MuonCand_charge[0] == MuonCand_charge[1]: continue
    
    acoplanarity = 1 - np.fabs(Pair_dphi[0]) / np.pi
    h_mumu_acop_mass.Fill(Pair_mass[0], acoplanarity)
    h_mumu_acop.Fill(acoplanarity)

    for j in range(2):
        h_muon_pt.Fill(MuonCand_pt[j])
        h_muon_eta.Fill(MuonCand_eta[j])
        h_muon_phi.Fill(MuonCand_phi[j])
        h_muon_e.Fill(MuonCand_e[j])

        h_muon_charge.Fill(MuonCand_charge[j])
        h_muon_istight.Fill(MuonCand_istight[j])

    h_pair_mass.Fill(Pair_mass[0])
    h_pair_pt.Fill(Pair_pt[0])
    h_pair_eta.Fill(Pair_eta[0])
    h_pair_phi.Fill(Pair_phi[0])
    h_pair_dpt.Fill(Pair_dpt[0])
    h_pair_dphi.Fill(Pair_dphi[0])

    #for j in range(nRecoProtCand[0]):
    #    h_prot_xi.Fill(ProtCand_xi[j])
    #    h_prot_arm.Fill(ProtCand_arm[j])
    #    h_prot_ismultirp.Fill(ProtCand_ismultirp[j])
    #    h_prot_rpid1.Fill(ProtCand_rpid1[j])


    # mu1.SetPtEtaPhiE(MuonCand_pt[0], MuonCand_eta[0], MuonCand_phi[0], MuonCand_e[0])
    # mu2.SetPtEtaPhiE(MuonCand_pt[1], MuonCand_eta[1], MuonCand_phi[1], MuonCand_e[1])
    # pair = mu1 + mu2
        
    # mumuxi_sol1 = (1/13000) * (MuonCand_pt[0] * np.exp(MuonCand_eta[0]) + MuonCand_pt[1] * np.exp(MuonCand_eta[1]))
    # mumuxi_sol2 = (1/13000) * (MuonCand_pt[0] * np.exp(-MuonCand_eta[0]) + MuonCand_pt[1] * np.exp(-MuonCand_eta[1]))


file = ROOT.TFile("histograms.root", "RECREATE")
h_muon_pt.Write()
h_muon_eta.Write()
h_muon_phi.Write()
h_muon_e.Write()
h_muon_charge.Write()
h_muon_istight.Write()

h_pair_mass.Write()
h_pair_pt.Write()
h_pair_eta.Write()
h_pair_phi.Write()
h_pair_dpt.Write()
h_pair_dphi.Write()

h_prot_xi.Write()
h_prot_arm.Write()
h_prot_ismultirp.Write()
h_prot_rpid1.Write()

h_mumu_acop.Write()
h_mumu_acop_mass.Write()
file.Close()


