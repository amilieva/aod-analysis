#define DimuonMacromoreProtUsed_cxx
#include "DimuonMacromoreProtUsed.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <ctime>
#include <sstream>

#define sqrts 13600.0
#define M_PI 3.14159265358979323846

void DimuonMacromoreProtUsed::Loop(Int_t multi, Int_t mc, Int_t sb, Int_t yr, Int_t nearfar)
{
//   In a ROOT session, you can do:
//      root> .L DimuonMacro.C
//      root> DimuonMacro m
//      root> m.GetEntry(12); // Fill t data members with entry number 12
//      root> m.Show();       // Show values of entry 12
//      root> m.Show(16);     // Read and show values of entry 16
//      root> m.Loop();       // Loop on all entries
//

   if (fChain == 0) return;

   TH1F *hacop = new TH1F("hacop","hacop",1000,0,0.01);
   TH2F *hcorr45 = new TH2F("hcorr45","hcorr45",500,0,0.25,500,0,0.25);
   TH2F *hcorr56 = new TH2F("hcorr56","hcorr56",500,0,0.25,500,0,0.25);

   TH2F *hcorrmult45 = new TH2F("hcorrmult45","hcorrmult45",500,0,0.25,500,0,0.25);
   TH2F *hcorrmult56 = new TH2F("hcorrmult56","hcorrmult56",500,0,0.25,500,0,0.25);

   TH1F *hres45 = new TH1F("hres45","hres45",300,-5,1);
   TH1F *hres56 = new TH1F("hres56","hres56",300,-5,1);
   TH1F *hressum = new TH1F("hressum","hressum",300,-5,1);

   TH1F *hbin145 = new TH1F("hbin145","hbin145",300,-5,1);
   TH1F *hbin156 = new TH1F("hbin156","hbin156",300,-5,1);
   TH1F *hbin1sum = new TH1F("hbin1sum","hbin1sum",300,-5,1);
   TH1F *hbin245 = new TH1F("hbin245","hbin245",300,-5,1);
   TH1F *hbin256 = new TH1F("hbin256","hbin256",300,-5,1);
   TH1F *hbin2sum = new TH1F("hbin2sum","hbin2sum",300,-5,1);
   TH1F *hbin345 = new TH1F("hbin345","hbin345",300,-5,1);
   TH1F *hbin356 = new TH1F("hbin356","hbin356",300,-5,1);
   TH1F *hbin3sum = new TH1F("hbin3sum","hbin3sum",300,-5,1);
   TH1F *hbin445 = new TH1F("hbin445","hbin445",300,-5,1);
   TH1F *hbin456 = new TH1F("hbin456","hbin456",300,-5,1);
   TH1F *hbin4sum = new TH1F("hbin4sum","hbin4sum",300,-5,1);

   TH1F *hbin3mult45 = new TH1F("hbin3mult45","hbin3mult45",300,-5,1);
   TH1F *hbin3mult56 = new TH1F("hbin3mult56","hbin3mult56",300,-5,1);
   TH1F *hbin4mult45 = new TH1F("hbin4mult45","hbin4mult45",300,-5,1);
   TH1F *hbin4mult56 = new TH1F("hbin4mult56","hbin4mult56",300,-5,1);


   TH1F *hres45mult = new TH1F("hres45mult","hres45mult",300,-5,1);
   TH1F *hres56mult = new TH1F("hres56mult","hres56mult",300,-5,1);
   TH1F *hressummult = new TH1F("hressummult","hressummult",300,-5,1);

   TH1F *hn45220 = new TH1F("hn45220","hn45220",10,0,10);
   TH1F *hn56220 = new TH1F("hn56220","hn56220",10,0,10);
   TH1F *hn45mult = new TH1F("hn45mult","hn45mult",10,0,10);
   TH1F *hn56mult = new TH1F("hn56mult","hn56mult",10,0,10);

   TH1F *hxi45mult = new TH1F("hxi45mult","hxi45mult",100,0,0.25);
   TH1F *hxi56mult = new TH1F("hxi56mult","hxi56mult",100,0,0.25);
   TH1F *hxangle45mult = new TH1F("hxangle45mult","hxangle45mult",50,120,170);
   TH1F *hxangle56mult = new TH1F("hxangle56mult","hxangle56mult",50,120,170);
   TH1F *hxi45single = new TH1F("hxi45single","hxi45single",100,0,0.25);
   TH1F *hxi56single = new TH1F("hxi56single","hxi56single",100,0,0.25);
   TH1F *hxangle45single = new TH1F("hxangle45single","hxangle45single",50,120,170);
   TH1F *hxangle56single = new TH1F("hxangle56single","hxangle56single",50,120,170);

   TH1F *hres451 = new TH1F("hres451","hres451",300,-5,1);
   TH1F *hres561 = new TH1F("hres561","hres561",300,-5,1);

   TH1F *hres45mult1 = new TH1F("hres45mult1","hres45mult1",300,-5,1);
   TH1F *hres56mult1 = new TH1F("hres56mult1","hres56mult1",300,-5,1);

   TH1F *hmresmulti = new TH1F("hmresmulti","hmresmulti",500,-15,5);
   TH1F *hmressingle = new TH1F("hmressingle","hmressingle",500,-15,5);
   TH1F *hmresmixed = new TH1F("hmresmixed","hmresmixed",500,-15,5);
   TH2F *hmcorrmulti = new TH2F("hmcorrmulti","hmcorrmulti",250,0,2500,250,0,2500);
   TH2F *hmcorrmixed = new TH2F("hmcorrmixed","hmcorrmixed",250,0,2500,250,0,2500);
   TH2F *hmcorrsingle = new TH2F("hmcorrsingle","hmcorrsingle",250,0,2500,250,0,2500);
   TH1F *hmmumu = new TH1F("hmmumu","hmmumu",250,0,2500);
   TH2F *hycorrmulti = new TH2F("hycorrmulti","hycorrmulti",250,-2.5,2.5,250,-2.5,2.5);
   TH2F *hycorrsingle = new TH2F("hycorrsingle","hycorrsingle",250,-2.5,2.5,250,-2.5,2.5);
   TH2F *hycorrmixed = new TH2F("hycorrmixed","hycorrmixed",250,-2.5,2.5,250,-2.5,2.5);
   TH2F *hdmdysingle = new TH2F("hdmdysingle","hdmdysingle",1000,-500,500,200,-2,2);

   TH1F *hmresycutsingle = new TH1F("hmresycutsingle","hmresycutsingle",500,-15,5);
   TH1F *hmresycutmulti = new TH1F("hmresycutmulti","hmresycutmulti",500,-15,5);
   TH1F *hmresycutmixed = new TH1F("hmresycutmixed","hmresycutmixed",500,-15,5);

   TH1F *hpzmumumultmatch45 = new TH1F("hpzmumumultmatch45","hpzmumumultmatch45",500,-2000,2000);
   TH1F *hpzmumusinglematch45 = new TH1F("hpzmumusinglematch45","hpzmumusinglematch45",500,-2000,2000);
   TH1F *hpzmumumultmatch56 = new TH1F("hpzmumumultmatch56","hpzmumumultmatch56",500,-2000,2000);
   TH1F *hpzmumusinglematch56 = new TH1F("hpzmumusinglematch56","hpzmumusinglematch56",500,-2000,2000);
   TH1F *hpzmumumultantimatch45 = new TH1F("hpzmumumultantimatch45","hpzmumumultantimatch45",500,-2000,2000);
   TH1F *hpzmumusingleantimatch45 = new TH1F("hpzmumusingleantimatch45","hpzmumusingleantimatch45",500,-2000,2000);
   TH1F *hpzmumumultantimatch56 = new TH1F("hpzmumumultantimatch56","hpzmumumultantimatch56",500,-2000,2000);
   TH1F *hpzmumusingleantimatch56 = new TH1F("hpzmumusingleantimatch56","hpzmumusingleantimatch56",500,-2000,2000);

   TH2F *hxivst45 = new TH2F("hxivst45","hxivst45",500,0,0.25,100,0,5);
   TH2F *hxivst56 = new TH2F("hxivst56","hxivst56",500,0,0.25,100,0,5);

   TH1F *h_muon_pt_1 = new TH1F("h_muon_pt_1","h_muon_pt_1",100,0,200);
   TH1F *h_muon_pt_2 = new TH1F("h_muon_pt_2","h_muon_pt_2",100,0,200);
   TH1F *h_pair_eta = new TH1F("h_pair_eta","h_pair_eta",100,-5,5);
   TH1F *h_pair_mass = new TH1F("h_pair_mass","h_pair_mass",100,0,200);

   TH2F *h_calculated_xi = new TH2F("h_calculated_xi","h_calculated_xi",100,0,0.25,100,0,0.25);
   TH1F *h_calculated_xi_1 = new TH1F("h_calculated_xi_1","h_calculated_xi_1",100,0,0.25);
   TH1F *h_calculated_xi_2 = new TH1F("h_calculated_xi_2","h_calculated_xi_2",100,0,0.25);

   TH2F *h_correlation_45 = new TH2F("h_correlation_45","h_correlation_45",100,0,0.25,100,0,0.25);
   TH2F *h_correlation_56 = new TH2F("h_correlation_56","h_correlation_56",100,0,0.25,100,0,0.25);
   TH2F *h_correlation_45_multi = new TH2F("h_correlation_45_multi","h_correlation_45_multi",100,0,0.25,100,0,0.25);
   TH2F *h_correlation_56_multi = new TH2F("h_correlation_56_multi","h_correlation_56_multi",100,0,0.25,100,0,0.25);

   TH1F *h_prot_xi_45 = new TH1F("h_prot_xi_45","h_prot_xi_45",100,0,0.25);
   TH1F *h_prot_xi_56 = new TH1F("h_prot_xi_56","h_prot_xi_56",100,0,0.25);
   TH1F *h_prot_xi_45_multi = new TH1F("h_prot_xi_45_multi","h_prot_xi_45_multi",100,0,0.25);
   TH1F *h_prot_xi_56_multi = new TH1F("h_prot_xi_56_multi","h_prot_xi_56_multi",100,0,0.25);

   TH1F *h_pair_extratracks0p25mm = new TH1F("h_pair_extratracks0p25mm","h_pair_extratracks0p25mm",10,0,10);
   TH1F *h_pair_extratracks0p3mm = new TH1F("h_pair_extratracks0p3mm","h_pair_extratracks0p3mm",10,0,10);
   TH1F *h_pair_extratracks0p5mm = new TH1F("h_pair_extratracks0p5mm","h_pair_extratracks0p5mm",10,0,10);
   TH1F *h_pair_extratracks2mm = new TH1F("h_pair_extratracks2mm","h_pair_extratracks2mm",10,0,10);
   TH1F *h_pair_extratracks5mm = new TH1F("h_pair_extratracks5mm","h_pair_extratracks5mm",10,0,10);
   TH1F *h_pair_extratracks2cm = new TH1F("h_pair_extratracks2cm","h_pair_extratracks2cm",10,0,10);
   TH1F *h_pair_extratracks10cm = new TH1F("h_pair_extratracks10cm","h_pair_extratracks10cm",10,0,10);

   ofstream singletxt("TextOutputSingleCorr.txt");
   ofstream multitxt("TextOutputMultiCorr.txt");

   singletxt << "Run,LS,Event,Arm,xing angle,xi(p),xi(mumu),ximatch,t,theta*x,theta*y" << std::endl;
   multitxt <<  "Run,LS,Event,Arm,xing angle,xi(p),xi(mumu),ximatch,t,theta*x,theta*y" << std::endl;

  //  ofstream test("ncount.txt")

   Int_t nent = fChain->GetEntries();

   TLorentzVector mu1,mu2,mumu;

   Int_t usemultitracks = 0;
   Int_t ismc = 1;
   Int_t issideband = 0;
   Int_t year = 2018;
   Int_t usenear = 0;

   /*
    * Basic options for the type of selction to apply
    */
   usemultitracks = multi; // Allow only 1 proton per arm, or multiple protons
   ismc = mc; // Run on data or MC
   issideband = sb; // Select signal or "sideband" in acoplanarity+multiplicity
   year = yr; // Year
   usenear = nearfar; // For SingleRP, choose near(210) or far(220)

   Int_t id45 = 23;
   Int_t id56 = 123;
   if(usenear == 1)
     {
       id45 = 3;
       id56 = 103;
     }

   Long64_t nentries = fChain->GetEntriesFast();
   Long64_t nbytes = 0, nb = 0;

   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      // if (Cut(ientry) < 0) continue;

      if(jentry % 10000 == 0)
	        std::cout << "Entry " << jentry << "/" << nent << std::endl;

      Float_t theacop = 1-fabs(Pair_dphi[0])/3.14159;

      // Basic selection cuts

      if(Pair_mass[0]<110) // M(mumu) > 110GeV
	      continue;
      if(MuonCand_pt[0]<20 || MuonCand_pt[1]<20) // pT(mu) > 20 GeV
        continue;
      if(MuonCand_istight[0]!=1 || MuonCand_istight[1]!=1) // Tight muon ID
        continue;
      if(MuonCand_charge[0] == MuonCand_charge[1]) // Opposite charge muons
        continue;
      if((theacop > 0.009) && (issideband == 0)) // Dimuon acoplanarity <=0.009 for signal
	      continue;
      if((theacop <= 0.009 || theacop > 0.1) && (issideband == 1)) // Acoplanarity 0.009-0.1 for sideband
        continue;
      //if((Pair_extratracks0p5mm[0] > 0) && (issideband == 0)) // 0-2 extra tracks within 0.5mm of dimuon vertex for signal
	    //  continue;
      // if((Pair_extratracks0p3mm[0] > 0) && (issideband == 0)) // 0-2 extra tracks within 0.25mm of dimuon vertex for signal
	    //   continue;
      if((Pair_extratracks1mm[0] > 0) && (issideband == 0)) // 0-2 extra tracks within 0.25mm of dimuon vertex for signal
	      continue;
      // if((Pair_extratracks0p5mm[0] > 0) && (issideband == 0)) // 0 extra tracks within 0.5mm of dimuon vertex for signal
	    //  continue;
      if((Pair_extratracks0p5mm[0] < 5 || Pair_extratracks0p5mm[0] > 10) && (issideband == 1)) // 5-10 extra tracks for sideband
        continue;

      mu1.SetPtEtaPhiE(MuonCand_pt[0],MuonCand_eta[0],MuonCand_phi[0],MuonCand_e[0]);
      mu2.SetPtEtaPhiE(MuonCand_pt[1],MuonCand_eta[1],MuonCand_phi[1],MuonCand_e[1]);
      mumu = mu1+mu2;

      h_muon_pt_1->Fill(MuonCand_pt[0]);
      h_muon_pt_2->Fill(MuonCand_pt[1]);
      h_pair_mass->Fill(Pair_mass[0]);
      h_pair_eta->Fill(Pair_eta[0]);

      h_pair_extratracks0p25mm->Fill(Pair_extratracks0p25mm[0]);
      h_pair_extratracks0p3mm->Fill(Pair_extratracks0p3mm[0]);
      h_pair_extratracks0p5mm->Fill(Pair_extratracks0p5mm[0]);
      h_pair_extratracks2mm->Fill(Pair_extratracks2mm[0]);
      h_pair_extratracks5mm->Fill(Pair_extratracks5mm[0]);
      h_pair_extratracks2cm->Fill(Pair_extratracks2cm[0]);
      h_pair_extratracks10cm->Fill(Pair_extratracks10cm[0]);

      hacop->Fill(1-fabs(Pair_dphi[0])/M_PI);

      Float_t mumuxisol1 = (1.0/sqrts)*((MuonCand_pt[0]*TMath::Exp(MuonCand_eta[0]) + MuonCand_pt[1]*TMath::Exp(MuonCand_eta[1])));
      Float_t mumuxisol2 = (1.0/sqrts)*((MuonCand_pt[0]*TMath::Exp(-1*MuonCand_eta[0]) + MuonCand_pt[1]*TMath::Exp(-1*MuonCand_eta[1])));

      h_calculated_xi->Fill(mumuxisol1,mumuxisol2);
      h_calculated_xi_1->Fill(mumuxisol1);
      h_calculated_xi_2->Fill(mumuxisol2);

      Float_t protxi45 = 0.0;
      Float_t protxi56 = 0.0;
      Float_t protxi45single = 0.0;
      Float_t protxi56single = 0.0;
      Float_t protxi45multi = 0.0;
      Float_t protxi56multi = 0.0;
      Float_t mumuxi45 = 0.0;
      Float_t mumuxi56 = 0.0;
      Float_t prott45 = 0.0;
      Float_t prott56 = 0.0;
      Float_t protthx45 = 0.0;
      Float_t protthy45 = 0.0;
      Float_t protthx56 = 0.0;
      Float_t protthy56 = 0.0;
      Int_t ntrk45 = 0;
      Int_t ntrk56 = 0;

      Int_t ncountpixel45 = 0;
      Int_t ncountpixel56 = 0;
      Int_t ncountmulti45 = 0;
      Int_t ncountmulti56 = 0;

      // Count the number of single RP protons per pot
      for(Int_t p = 0; p < nRecoProtCand; p++)
        {
          if(ProtCand_rpid1[p] == id45 && ProtCand_ismultirp[p]==0)
            ncountpixel45++;
          if(ProtCand_rpid1[p] == id56 && ProtCand_ismultirp[p]==0)
            ncountpixel56++;
          if(ProtCand_ismultirp[p] == 1 && ProtCand_arm[p] == 0)
            ncountmulti45++;
          if(ProtCand_ismultirp[p] == 1 && ProtCand_arm[p] == 1)
            ncountmulti56++;
        }

      hn45220->Fill(ncountpixel45);
      hn56220->Fill(ncountpixel56);
      hn45mult->Fill(ncountmulti45);
      hn56mult->Fill(ncountmulti56);

      // Now do the actual analysis loop
      for(Int_t p = 0; p < nRecoProtCand; p++)
        {
          // std::cout<<"nRecoProtCand = "<<nRecoProtCand<<std::endl;
          // std::cout<<"ncountpixel45 = "<<ncountpixel45<<std::endl;
          // std::cout<<"ncountpixel56 = "<<ncountpixel56<<std::endl;
          // std::cout<<"ncountmulti45 = "<<ncountmulti45<<std::endl;
          // std::cout<<"ncountmulti56 = "<<ncountmulti56<<std::endl;

          // continue;
	        // Sector 45, singleRP
          // if(ProtCand_rpid1[p] == id45 && ProtCand_ismultirp[p] == 0 && ntrk45 < 1 && (ncountpixel45 == 1 || usemultitracks == 0))
          if(ProtCand_rpid1[p] == id45 && ProtCand_ismultirp[p] == 0 && ntrk45 <= 3 && (ncountpixel45 <= 3 || usemultitracks == 0))

          for (Int_t q = 0; q < ncountpixel45; q++)
            {
              protxi45 = ProtCand_xi[p];
              protxi45single = protxi45;
              hcorr45->Fill(protxi45,mumuxisol1);

              h_prot_xi_45->Fill(protxi45);

              if(mumuxisol1 >= 0.02)
                h_correlation_45->Fill(protxi45,mumuxisol1);
                hres451->Fill(1 - (protxi45/mumuxisol1)); 
                
              hres45->Fill(1 - (protxi45/mumuxisol1));
              hressum->Fill(1 - (protxi45/mumuxisol1));

              if(mumuxisol1 >= 0.02 && mumuxisol1 < 0.03)
                hbin145->Fill(1 - (protxi45/mumuxisol1));
              if(mumuxisol1 >= 0.03 && mumuxisol1 < 0.04)
                hbin245->Fill(1 - (protxi45/mumuxisol1));
              if(mumuxisol1 >= 0.04 && mumuxisol1 < 0.06)
                hbin345->Fill(1 - (protxi45/mumuxisol1));
              if(mumuxisol1 >= 0.06)
                hbin445->Fill(1 - (protxi45/mumuxisol1));

              if(fabs(1 - (protxi45/mumuxisol1)) < 0.25)
                {
                  hxi45single->Fill(protxi45);
                  hpzmumusinglematch45->Fill(mumu.Pz());
                }
              else
                hpzmumusingleantimatch45->Fill(mumu.Pz());

              std::cout << "Run, Lumi, Event = " << Run << ", " << LumiSection << ", " << EventNum << std::endl;
              std::cout << "\t xi = " << protxi45 << std::endl;

              if (usenear)
                singletxt << Run << " " << LumiSection << " " << EventNum << " Single 3  " << protxi45 << " " << (1 - (protxi45/mumuxisol1)) << std::endl;     
              else
                singletxt << Run << " " << LumiSection << " " << EventNum << " Single 23 " << protxi45 << " " << (1 - (protxi45/mumuxisol1)) << std::endl;     
              
              ntrk45++;
            }
          // Sector 56, singleRP           
          // if(ProtCand_rpid1[p] == id56 && ProtCand_ismultirp[p] == 0 && ntrk56 < 1 && (ncountpixel56 == 1 || usemultitracks == 0))                                                                                                                                      
          if(ProtCand_rpid1[p] == id56 && ProtCand_ismultirp[p] == 0 && ntrk56 <= 3 && (ncountpixel56 <= 3 || usemultitracks == 0))

          // should we use the number of tracks in the pixel detector to limit the number of tracks in the singleRP/multiRP?
          // can we just use if(ProtCand_rpid1[p] == id56 && ProtCand_ismultirp[p] == 0 && ntrk56 <= 3 && usemultitracks == 0)??

            {
              protxi56 = ProtCand_xi[p];
              protxi56single = protxi56;
              hcorr56->Fill(protxi56,mumuxisol2);
              hres56->Fill(1 - (protxi56/mumuxisol2));
              hressum->Fill(1 - (protxi56/mumuxisol2));

              h_prot_xi_56->Fill(protxi56);

              if(mumuxisol2 >= 0.02)
                h_correlation_56->Fill(protxi56,mumuxisol2);
                hres561->Fill(1 - (protxi56/mumuxisol2));

              if(mumuxisol2 >= 0.02 && mumuxisol2 < 0.03)
                hbin156->Fill(1 - (protxi56/mumuxisol2));
              if(mumuxisol2 >= 0.03 && mumuxisol2 < 0.04)
                hbin256->Fill(1 - (protxi56/mumuxisol2));
              if(mumuxisol2 >= 0.04 && mumuxisol2 < 0.06)
                hbin356->Fill(1 - (protxi56/mumuxisol2));
              if(mumuxisol2 >= 0.06)
                hbin456->Fill(1 - (protxi56/mumuxisol2));

              if(fabs(1 - (protxi56/mumuxisol2)) < 0.25)
                {
                  hxi56single->Fill(protxi56);
                  hpzmumusinglematch56->Fill(mumu.Pz());
                }
              else
                hpzmumusingleantimatch56->Fill(mumu.Pz());

              std::cout << "Run, Lumi, Event = " << Run << " " << LumiSection << " " << EventNum << std::endl;
              std::cout << "\t xi = " <<protxi56 << std::endl;

              if (usenear)
                singletxt << Run << " " << LumiSection << " " << EventNum << " Single 103 " << protxi56 << " " << (1 - (protxi56/mumuxisol2)) << std::endl;     
              else
                singletxt << Run << " " << LumiSection << " " << EventNum << " Single 123 " << protxi56 << " " << (1 - (protxi56/mumuxisol2)) << std::endl;     
              
              ntrk56++;
            }
          // Sector 45, multiRP                                                                                                                                                   
          if(ProtCand_arm[p] == 0 && ProtCand_ismultirp[p] == 1 && (ncountmulti45 == 1 || usemultitracks == 1))
            {
              protxi45 = ProtCand_xi[p];
              prott45 = -1.0*ProtCand_t[p];
              protthx45 = ProtCand_ThX[p];
              protthy45 = ProtCand_ThY[p];
              protxi45multi = protxi45;

              h_prot_xi_45_multi->Fill(protxi45);

              // Tracks for eff. correction                                                                                                                                      
              // protx45multi220 = ProtCand_trackx1[p];
              // proty45multi220 = ProtCand_tracky1[p];
              // protx45multi210 = ProtCand_trackx2[p];
              // proty45multi210 = ProtCand_tracky2[p];

              hres45mult->Fill(1 - (protxi45/mumuxisol1));
              hressummult->Fill(1 - (protxi45/mumuxisol1));
              hcorrmult45->Fill(protxi45,mumuxisol1);

              if(mumuxisol1 >= 0.02)
                  h_correlation_45_multi->Fill(protxi45,mumuxisol1);
                  hres45mult1->Fill(1 - (protxi45/mumuxisol1));

              if(mumuxisol1 >= 0.04 && mumuxisol1 < 0.06)
                  hbin3mult45->Fill(1 - (protxi45/mumuxisol1));
              if(mumuxisol1 >= 0.06)
                  hbin4mult45->Fill(1 - (protxi45/mumuxisol1));

              std::cout << "Run, Lumi, Event = " << Run << ", " << LumiSection << ", " << EventNum << std::endl;
              std::cout << "\t xi(multi arm=45) = " << protxi45 << std::endl;

              if(fabs(1 - (protxi45/mumuxisol1)) < 0.10 && (mumuxisol1 >= 0.04))
              {
                hxi45mult->Fill(protxi45);
                hxangle45mult->Fill(CrossingAngle);
                hpzmumumultmatch45->Fill(mumu.Pz());
                hxivst45->Fill(protxi45,prott45);

                multitxt << Run << "," << LumiSection << "," << EventNum << ",45," << CrossingAngle << "," << protxi45 << "," << mumuxisol1 << ","
                    << 1 - (protxi45/mumuxisol1) << "," << prott45
                    << ", " << protthx45 << ", " << protthy45 << ", " << ", " 
                    << std::endl;
              }
              else
              hpzmumumultantimatch45->Fill(mumu.Pz());
          }
          // Sector 56, multiRP
          if(ProtCand_arm[p] == 1 && ProtCand_ismultirp[p] == 1 && (ncountmulti56==1 || usemultitracks==1))
          {
            protxi56 = ProtCand_xi[p];
            prott56 = -1.0*ProtCand_t[p];
            protthx56 = ProtCand_ThX[p];
            protthy56 = ProtCand_ThY[p];
            protxi56multi = protxi56;

            h_prot_xi_56_multi->Fill(protxi56);
            
            // Tracks for eff. correction                                                                                                                                      
            // protx56multi220 = ProtCand_trackx1[p];
            // proty56multi220 = ProtCand_tracky1[p];
            // protx56multi210 = ProtCand_trackx2[p];
            // proty56multi210 = ProtCand_tracky2[p];
            
            hres56mult->Fill(1 - (protxi56/mumuxisol2));
            hressummult->Fill(1 - (protxi56/mumuxisol2));
            hcorrmult56->Fill(protxi56,mumuxisol2);

            if(mumuxisol2 >= 0.02)
                h_correlation_56_multi->Fill(protxi56,mumuxisol2);
                hres56mult1->Fill(1 - (protxi56/mumuxisol2));
            
            if(mumuxisol2 >= 0.04 && mumuxisol2 < 0.06)
                hbin3mult56->Fill(1 - (protxi56/mumuxisol2));
            if(mumuxisol2 >= 0.06)
                hbin4mult56->Fill(1 - (protxi56/mumuxisol2));
            
            
            std::cout << "Run, Lumi, Event = " << Run << ", " << LumiSection << ", " << EventNum << std::endl;
            std::cout << "\t xi(multi arm=56) = " << protxi56 << std::endl;
            
            if((fabs(1 - (protxi56/mumuxisol2)) > -0.05) && (fabs(1 - (protxi56/mumuxisol2)) < 0.20) && (mumuxisol2 >= 0.04))
            {
              hxi56mult->Fill(protxi56);
              hxangle56mult->Fill(CrossingAngle);
              hpzmumumultmatch56->Fill(mumu.Pz());
              hxivst56->Fill(protxi56,prott56);
              
              multitxt << Run << "," << LumiSection << "," << EventNum << ",56," << CrossingAngle << "," << protxi56 << "," << mumuxisol2 << ","
                  << 1 - (protxi56/mumuxisol2) << "," << prott56
                  << ", " << protthx56 << ", " << protthy56 << ", " << ", " 
                  << std::endl;
              
            }
            else
              hpzmumumultantimatch56->Fill(mumu.Pz());
          }
	      }
  }

   // Textfile output
   singletxt.close();
   multitxt.close();
          
   // ROOT histogram output
   TFile *fx;

   std::time_t t = std::time(nullptr);
   std::tm* now = std::localtime(&t);

   std::stringstream date_stream;
   date_stream << (now->tm_year + 1900) << '-'
                << (now->tm_mon + 1) << '-'
                << now->tm_mday;
   std::string date_str = date_stream.str();

   std::string filename = date_str + "-DimuonHistograms-ximu2percent-extratracks1-";

   if(multi == 1)
     filename += "multi-";
   else
     filename += "single-";

   if(issideband == 0)
     filename += "signal";
   else
     filename += "sideband";

     if (multi == 0)
      if (usenear == 1)
        filename += "-near.root";
      else
        filename += "-far.root";
    else
      filename += ".root";

    std::cout << "Creating output file: " << filename << std::endl;


   fx = new TFile(filename.c_str(),"RECREATE");

   // Titles and axis labels
   hacop->SetTitle("Acoplanarity");
   hacop->GetXaxis()->SetTitle("1 - |#Delta#phi|/#pi");
   hacop->GetYaxis()->SetTitle("Events");

   hcorr45->SetTitle("Proton #xi vs. #xi_{mumu} (SingleRP 45)");
   hcorr45->GetXaxis()->SetTitle("Proton #xi");
   hcorr45->GetYaxis()->SetTitle("#xi_{mumu}");

   hcorr56->SetTitle("Proton #xi vs. #xi_{mumu} (SingleRP 56)");
   hcorr56->GetXaxis()->SetTitle("Proton #xi");
   hcorr56->GetYaxis()->SetTitle("#xi_{mumu}");

   hcorrmult45->SetTitle("Proton #xi vs. #xi_{mumu} (MultiRP 45)");
   hcorrmult45->GetXaxis()->SetTitle("Proton #xi");
   hcorrmult45->GetYaxis()->SetTitle("#xi_{mumu}");

   hcorrmult56->SetTitle("Proton #xi vs. #xi_{mumu} (MultiRP 56)");
   hcorrmult56->GetXaxis()->SetTitle("Proton #xi");
   hcorrmult56->GetYaxis()->SetTitle("#xi_{mumu}");

   hres45->SetTitle("Resolution (SingleRP 45)");
   hres45->GetXaxis()->SetTitle("1 - #xi_{p}/#xi_{mumu}");
   hres45->GetYaxis()->SetTitle("Events");

   hres56->SetTitle("Resolution (SingleRP 56)");
   hres56->GetXaxis()->SetTitle("1 - #xi_{p}/#xi_{mumu}");
   hres56->GetYaxis()->SetTitle("Events");

   hressum->SetTitle("Resolution (Sum)");
   hressum->GetXaxis()->SetTitle("1 - #xi_{p}/#xi_{mumu}");
   hressum->GetYaxis()->SetTitle("Events");

   hres45mult->SetTitle("Resolution (MultiRP 45)");
   hres45mult->GetXaxis()->SetTitle("1 - #xi_{p}/#xi_{mumu}");
   hres45mult->GetYaxis()->SetTitle("Events");

   hres56mult->SetTitle("Resolution (MultiRP 56)");
   hres56mult->GetXaxis()->SetTitle("1 - #xi_{p}/#xi_{mumu}");
   hres56mult->GetYaxis()->SetTitle("Events");

   hressummult->SetTitle("Resolution (Sum)");
   hressummult->GetXaxis()->SetTitle("1 - #xi_{p}/#xi_{mumu}");
   hressummult->GetYaxis()->SetTitle("Events");

   hn45220->SetTitle("SingleRP 45 Proton Count");
   hn45220->GetXaxis()->SetTitle("Number of protons");
   hn45220->GetYaxis()->SetTitle("Events");

   hn56220->SetTitle("SingleRP 56 Proton Count");
   hn56220->GetXaxis()->SetTitle("Number of protons");
   hn56220->GetYaxis()->SetTitle("Events");

   hn45mult->SetTitle("MultiRP 45 Proton Count");
   hn45mult->GetXaxis()->SetTitle("Number of protons");
   hn45mult->GetYaxis()->SetTitle("Events");

   hn56mult->SetTitle("MultiRP 56 Proton Count");
   hn56mult->GetXaxis()->SetTitle("Number of protons");
   hn56mult->GetYaxis()->SetTitle("Events");

   hxi45mult->SetTitle("Proton #xi (MultiRP 45)");
   hxi45mult->GetXaxis()->SetTitle("#xi_{p}");
   hxi45mult->GetYaxis()->SetTitle("Events");

   hxi56mult->SetTitle("Proton #xi (MultiRP 56)");
   hxi56mult->GetXaxis()->SetTitle("#xi_{p}");
   hxi56mult->GetYaxis()->SetTitle("Events");

   hxangle45mult->SetTitle("Proton #theta_{x} (MultiRP 45)");
   hxangle45mult->GetXaxis()->SetTitle("#theta_{x}");
   hxangle45mult->GetYaxis()->SetTitle("Events");

   hxangle56mult->SetTitle("Proton #theta_{x} (MultiRP 56)");
   hxangle56mult->GetXaxis()->SetTitle("#theta_{x}");
   hxangle56mult->GetYaxis()->SetTitle("Events");

   hxi45single->SetTitle("Proton #xi (SingleRP 45)");
   hxi45single->GetXaxis()->SetTitle("#xi");
   hxi45single->GetYaxis()->SetTitle("Events");

   hxi56single->SetTitle("Proton #xi (SingleRP 56)");
   hxi56single->GetXaxis()->SetTitle("#xi");
   hxi56single->GetYaxis()->SetTitle("Events");

   hmresmulti->SetTitle("Resolution (MultiRP)");
   hmresmulti->GetXaxis()->SetTitle("1 - #xi_{p}/#xi_{mumu}");
   hmresmulti->GetYaxis()->SetTitle("Events");

   hmressingle->SetTitle("Resolution (SingleRP)");
   hmressingle->GetXaxis()->SetTitle("1 - #xi_{p}/#xi_{mumu}");
   hmressingle->GetYaxis()->SetTitle("Events");

   hmresmixed->SetTitle("Resolution (Mixed)");
   hmresmixed->GetXaxis()->SetTitle("1 - #xi_{p}/#xi_{mumu}");
   hmresmixed->GetYaxis()->SetTitle("Events");

   hmcorrmulti->SetTitle("Proton #xi vs. #xi_{mumu} (MultiRP)");
   hmcorrmulti->GetXaxis()->SetTitle("Proton #xi");
   hmcorrmulti->GetYaxis()->SetTitle("#xi_{mumu}");

   hmcorrsingle->SetTitle("Proton #xi vs. #xi_{mumu} (SingleRP)");
   hmcorrsingle->GetXaxis()->SetTitle("Proton #xi");
   hmcorrsingle->GetYaxis()->SetTitle("#xi_{mumu}");

   hmcorrmixed->SetTitle("Proton #xi vs. #xi_{mumu} (Mixed)");
   hmcorrmixed->GetXaxis()->SetTitle("Proton #xi");
   hmcorrmixed->GetYaxis()->SetTitle("#xi_{mumu}");

   hmmumu->SetTitle("Dimuon Pz");
   hmmumu->GetXaxis()->SetTitle("Pz");
   hmmumu->GetYaxis()->SetTitle("Events");

   hycorrsingle->SetTitle("Proton y vs. y_{mumu} (SingleRP)");
   hycorrsingle->GetXaxis()->SetTitle("Proton y");
   hycorrsingle->GetYaxis()->SetTitle("y_{mumu}");

   hycorrmulti->SetTitle("Proton y vs. y_{mumu} (MultiRP)");
   hycorrmulti->GetXaxis()->SetTitle("Proton y");
   hycorrmulti->GetYaxis()->SetTitle("y_{mumu}");

   hycorrmixed->SetTitle("Proton y vs. y_{mumu} (Mixed)");
   hycorrmixed->GetXaxis()->SetTitle("Proton y");
   hycorrmixed->GetYaxis()->SetTitle("y_{mumu}");

   hdmdysingle->SetTitle("Proton #xi vs. #xi_{mumu} (SingleRP)");
   hdmdysingle->GetXaxis()->SetTitle("Proton #xi");
   hdmdysingle->GetYaxis()->SetTitle("#xi_{mumu}");

   hmresycutsingle->SetTitle("Resolution (SingleRP)");
   hmresycutsingle->GetXaxis()->SetTitle("1 - #xi_{p}/#xi_{mumu}");
   hmresycutsingle->GetYaxis()->SetTitle("Events");

   hmresycutmulti->SetTitle("Resolution (MultiRP)");
   hmresycutmulti->GetXaxis()->SetTitle("1 - #xi_{p}/#xi_{mumu}");
   hmresycutmulti->GetYaxis()->SetTitle("Events");

   hmresycutmixed->SetTitle("Resolution (Mixed)");
   hmresycutmixed->GetXaxis()->SetTitle("1 - #xi_{p}/#xi_{mumu}");
   hmresycutmixed->GetYaxis()->SetTitle("Events");

   hpzmumusinglematch45->SetTitle("Dimuon Pz (SingleRP 45)");
   hpzmumusinglematch45->GetXaxis()->SetTitle("Pz");
   hpzmumusinglematch45->GetYaxis()->SetTitle("Events");

   hpzmumusinglematch56->SetTitle("Dimuon Pz (SingleRP 56)");
   hpzmumusinglematch56->GetXaxis()->SetTitle("Pz");
   hpzmumusinglematch56->GetYaxis()->SetTitle("Events");

   hpzmumumultmatch45->SetTitle("Dimuon Pz (MultiRP 45)");
   hpzmumumultmatch45->GetXaxis()->SetTitle("Pz");
   hpzmumumultmatch45->GetYaxis()->SetTitle("Events");

   hpzmumumultmatch56->SetTitle("Dimuon Pz (MultiRP 56)");
   hpzmumumultmatch56->GetXaxis()->SetTitle("Pz");
   hpzmumumultmatch56->GetYaxis()->SetTitle("Events");

   hpzmumusingleantimatch45->SetTitle("Dimuon Pz (SingleRP 45)");
   hpzmumusingleantimatch45->GetXaxis()->SetTitle("Pz");
   hpzmumusingleantimatch45->GetYaxis()->SetTitle("Events");

   hpzmumusingleantimatch56->SetTitle("Dimuon Pz (SingleRP 56)");
   hpzmumusingleantimatch56->GetXaxis()->SetTitle("Pz");
   hpzmumusingleantimatch56->GetYaxis()->SetTitle("Events");

   hxivst45->SetTitle("Proton #xi vs. t (MultiRP 45)");
   hxivst45->GetXaxis()->SetTitle("#xi");
   hxivst45->GetYaxis()->SetTitle("t");

   hxivst56->SetTitle("Proton #xi vs. t (MultiRP 56)");
   hxivst56->GetXaxis()->SetTitle("#xi");
   hxivst56->GetYaxis()->SetTitle("t");

   h_muon_pt_1->SetTitle("Muon pT 1");
   h_muon_pt_1->GetXaxis()->SetTitle("pT");
   h_muon_pt_1->GetYaxis()->SetTitle("Events");

   
   h_muon_pt_2->SetTitle("Muon pT 2");
   h_muon_pt_2->GetXaxis()->SetTitle("pT");
   h_muon_pt_2->GetYaxis()->SetTitle("Events");

   h_pair_mass->SetTitle("Dimuon Mass");
   h_pair_mass->GetXaxis()->SetTitle("Mass");
   h_pair_mass->GetYaxis()->SetTitle("Events");

   h_pair_eta->SetTitle("Dimuon Eta");
   h_pair_eta->GetXaxis()->SetTitle("Eta");
   h_pair_eta->GetYaxis()->SetTitle("Events");

   h_calculated_xi->SetTitle("#xi_{mumu1} vs. #xi_{mumu2}");
   h_calculated_xi->GetXaxis()->SetTitle("#xi_{mumu1}");
   h_calculated_xi->GetYaxis()->SetTitle("#xi_{mumu2}");

   h_calculated_xi_1->SetTitle("#xi_{mumu1}");
   h_calculated_xi_1->GetXaxis()->SetTitle("#xi_{mumu1}");
   h_calculated_xi_1->GetYaxis()->SetTitle("Events");

   h_calculated_xi_2->SetTitle("#xi_{mumu2}");
   h_calculated_xi_2->GetXaxis()->SetTitle("#xi_{mumu2}");
   h_calculated_xi_2->GetYaxis()->SetTitle("Events");

   h_correlation_45->SetTitle("Proton #xi vs. #xi_{mumu} (SingleRP 45)");
   h_correlation_45->GetXaxis()->SetTitle("Proton #xi");
   h_correlation_45->GetYaxis()->SetTitle("#xi_{mumu}");

   h_correlation_56->SetTitle("Proton #xi vs. #xi_{mumu} (SingleRP 56)");
   h_correlation_56->GetXaxis()->SetTitle("Proton #xi");
   h_correlation_56->GetYaxis()->SetTitle("#xi_{mumu}");

   h_correlation_45_multi->SetTitle("Proton #xi vs. #xi_{mumu} (MultiRP 45)");
   h_correlation_45_multi->GetXaxis()->SetTitle("Proton #xi");
   h_correlation_45_multi->GetYaxis()->SetTitle("#xi_{mumu}");

   h_correlation_56_multi->SetTitle("Proton #xi vs. #xi_{mumu} (MultiRP 56)");
   h_correlation_56_multi->GetXaxis()->SetTitle("Proton #xi");
   h_correlation_56_multi->GetYaxis()->SetTitle("#xi_{mumu}");

   h_prot_xi_45->SetTitle("Proton #xi (SingleRP 45)");
   h_prot_xi_45->GetXaxis()->SetTitle("#xi");
   h_prot_xi_45->GetYaxis()->SetTitle("Events");

   h_prot_xi_56->SetTitle("Proton #xi (SingleRP 56)");
   h_prot_xi_56->GetXaxis()->SetTitle("#xi");
   h_prot_xi_56->GetYaxis()->SetTitle("Events");

   h_prot_xi_45_multi->SetTitle("Proton #xi (MultiRP 45)");
   h_prot_xi_45_multi->GetXaxis()->SetTitle("#xi");
   h_prot_xi_45_multi->GetYaxis()->SetTitle("Events");

   h_prot_xi_56_multi->SetTitle("Proton #xi (MultiRP 56)");
   h_prot_xi_56_multi->GetXaxis()->SetTitle("#xi");
   h_prot_xi_56_multi->GetYaxis()->SetTitle("Events");

   h_pair_extratracks0p5mm->SetTitle("Extra tracks within 0.5mm of dimuon vertex");
   h_pair_extratracks0p5mm->GetXaxis()->SetTitle("Number of tracks");
   h_pair_extratracks0p5mm->GetYaxis()->SetTitle("Events");

   h_pair_extratracks2mm->SetTitle("Extra tracks within 2mm of dimuon vertex");
   h_pair_extratracks2mm->GetXaxis()->SetTitle("Number of tracks");
   h_pair_extratracks2mm->GetYaxis()->SetTitle("Events");

   h_pair_extratracks5mm->SetTitle("Extra tracks within 5mm of dimuon vertex");
   h_pair_extratracks5mm->GetXaxis()->SetTitle("Number of tracks");
   h_pair_extratracks5mm->GetYaxis()->SetTitle("Events");

   h_pair_extratracks2cm->SetTitle("Extra tracks within 2cm of dimuon vertex");
   h_pair_extratracks2cm->GetXaxis()->SetTitle("Number of tracks");
   h_pair_extratracks2cm->GetYaxis()->SetTitle("Events");

   h_pair_extratracks10cm->SetTitle("Extra tracks within 10cm of dimuon vertex");
   h_pair_extratracks10cm->GetXaxis()->SetTitle("Number of tracks");
   h_pair_extratracks10cm->GetYaxis()->SetTitle("Events");   

   hres45mult1->SetTitle("Resolution (MultiRP 45)");
    hres45mult1->GetXaxis()->SetTitle("1 - #xi_{p}/#xi_{mumu}");
    hres45mult1->GetYaxis()->SetTitle("Events");

    hres56mult1->SetTitle("Resolution (MultiRP 56)");
    hres56mult1->GetXaxis()->SetTitle("1 - #xi_{p}/#xi_{mumu}");
    hres56mult1->GetYaxis()->SetTitle("Events");

   hres451->SetTitle("Resolution (SingleRP 45)");
   hres451->GetXaxis()->SetTitle("1 - #xi_{p}/#xi_{mumu}");
   hres451->GetYaxis()->SetTitle("Events");
 
   hres561->SetTitle("Resolution (SingleRP 56)");
   hres561->GetXaxis()->SetTitle("1 - #xi_{p}/#xi_{mumu}");
   hres561->GetYaxis()->SetTitle("Events");

   h_pair_extratracks0p25mm->SetTitle("Extra tracks within 0.25mm of dimuon vertex");
    h_pair_extratracks0p25mm->GetXaxis()->SetTitle("Number of tracks");
    h_pair_extratracks0p25mm->GetYaxis()->SetTitle("Events");

    h_pair_extratracks0p3mm->SetTitle("Extra tracks within 0.3mm of dimuon vertex");
    h_pair_extratracks0p3mm->GetXaxis()->SetTitle("Number of tracks");
    h_pair_extratracks0p3mm->GetYaxis()->SetTitle("Events");




   // Write all histograms
   hacop->Write();
   hcorr45->Write();
   hres45->Write();
   hcorr56->Write();
   hres56->Write();
   hressum->Write();
   hres45mult->Write();
   hres56mult->Write();
   hressummult->Write();
   
   hbin145->Write();
   hbin245->Write();
   hbin345->Write();
   hbin445->Write();

   hbin156->Write();
   hbin256->Write();
   hbin356->Write();
   hbin456->Write();

   hbin3mult45->Write();
   hbin3mult56->Write();
   hbin4mult45->Write();
   hbin4mult56->Write();

   hn45220->Write();
   hn56220->Write();
   hn45mult->Write();
   hn56mult->Write();

   hcorrmult45->Write();
   hcorrmult56->Write();

   hxi45mult->Write();
   hxi56mult->Write();
   hxangle45mult->Write();
   hxangle56mult->Write();
   hxi45single->Write();
   hxi56single->Write();

   hmresmulti->Write();
   hmresmixed->Write();
   hmressingle->Write();
   hmcorrsingle->Write();
   hmcorrmulti->Write();
   hmcorrmixed->Write();

   hmmumu->Write();
   hycorrsingle->Write();
   hycorrmulti->Write();
   hycorrmixed->Write();
   hdmdysingle->Write();
   hmresycutmulti->Write();
   hmresycutmixed->Write();
   hmresycutsingle->Write();
   
   hpzmumusinglematch45->Write();
   hpzmumusinglematch56->Write();
   hpzmumumultmatch45->Write();
   hpzmumumultmatch56->Write();
   hpzmumusingleantimatch45->Write();
   hpzmumusingleantimatch56->Write();
   hpzmumumultantimatch45->Write();
   hpzmumumultantimatch56->Write();

   hxivst45->Write();
   hxivst56->Write();

   h_muon_pt_1->Write();
   h_muon_pt_2->Write();
   h_pair_mass->Write();
   h_pair_eta->Write();

   h_calculated_xi->Write();
   h_calculated_xi_1->Write();
   h_calculated_xi_2->Write();

   h_correlation_45->Write();
   h_correlation_56->Write();
   h_correlation_45_multi->Write();
   h_correlation_56_multi->Write();

   h_prot_xi_45->Write();
   h_prot_xi_56->Write();
   h_prot_xi_45_multi->Write();
   h_prot_xi_56_multi->Write();

   h_pair_extratracks0p5mm->Write();
   h_pair_extratracks2mm->Write();
   h_pair_extratracks5mm->Write();
   h_pair_extratracks2cm->Write();
   h_pair_extratracks10cm->Write();

   hres45mult1->Write();
    hres56mult1->Write();
   hres451->Write();
   hres561->Write();

    h_pair_extratracks0p25mm->Write();
    h_pair_extratracks0p3mm->Write();

   fx->Write();

}
