import ROOT
import sys

def list_root_file_contents(file_path):
    file = ROOT.TFile.Open(file_path)
    if not file or file.IsZombie():
        print(f"Error: Cannot open file {file_path}")
        return
    
    print(f"Listing contents of file: {file_path}")
    file.ls()
    file.Close()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python list_root_file_contents.py <ROOTfile>")
    else:
        list_root_file_contents(sys.argv[1])