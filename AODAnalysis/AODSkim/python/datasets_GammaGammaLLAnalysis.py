import sys,os
JSONPATH = os.path.realpath(os.path.join(os.getcwd(),"../data"))

# The name of this file MUST contain the analysis name: datasets_<name>Analysis.py.
# E.g. NanoAOD_Hplus2taunuAnalysisSkim.py -> datasets_Hplus2taunuAnalysis.py
# lumijsons /eos/home-c/cmsdqm/www/CAF/certification/Collisions22

from Dataset import Dataset
"""
class Dataset:
    def __init__(self, url, dbs="global", dataVersion="94Xmc", lumiMask="", name=""):
        self.URL = url
        self.DBS = dbs
        self.dataVersion = dataVersion
        self.lumiMask = lumiMask
        self.name = name
        self.files = []

    def isData(self):
        if "Run20" in self.URL:
            return True
        return False

    def getName(self):
        return self.name

    def getYear(self):
        year_re = re.compile("/Run(?P<year>20\d\d)\S")
        match = year_re.search(self.URL)
        if match:
            return match.group("year")
        else:
            return None

    def setFiles(self, files):
        self.files = files

    def getFiles(self):
        return self.files
"""

datasets = {}

datasets["2023"] = []

datasets["2023"].append(Dataset('/Muon0/Run2023B-PromptReco-v1/AOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"PPS-July2023_track_only_AND_CMS_Golden.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023B-PromptReco-v1/AOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"PPS-July2023_track_only_AND_CMS_Golden.json")))


datasets["2023"].append(Dataset('/Muon0/Run2023C-PromptReco-v1/AOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"PPS-July2023_track_only_AND_CMS_Golden.json")))
datasets["2023"].append(Dataset('/Muon0/Run2023C-PromptReco-v2/AOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"PPS-July2023_track_only_AND_CMS_Golden.json")))
datasets["2023"].append(Dataset('/Muon0/Run2023C-PromptReco-v3/AOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"PPS-July2023_track_only_AND_CMS_Golden.json")))
datasets["2023"].append(Dataset('/Muon0/Run2023C-PromptReco-v4/AOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"PPS-July2023_track_only_AND_CMS_Golden.json")))

datasets["2023"].append(Dataset('/Muon1/Run2023C-PromptReco-v1/AOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"PPS-July2023_track_only_AND_CMS_Golden.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023C-PromptReco-v2/AOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"PPS-July2023_track_only_AND_CMS_Golden.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023C-PromptReco-v3/AOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"PPS-July2023_track_only_AND_CMS_Golden.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023C-PromptReco-v4/AOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"PPS-July2023_track_only_AND_CMS_Golden.json")))


datasets["2023"].append(Dataset('/Muon0/Run2023D-PromptReco-v1/AOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"PPS-July2023_track_only_AND_CMS_Golden.json")))
datasets["2023"].append(Dataset('/Muon0/Run2023D-PromptReco-v2/AOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"PPS-July2023_track_only_AND_CMS_Golden.json")))

datasets["2023"].append(Dataset('/Muon1/Run2023D-PromptReco-v1/AOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"PPS-July2023_track_only_AND_CMS_Golden.json")))
datasets["2023"].append(Dataset('/Muon1/Run2023D-PromptReco-v2/AOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"PPS-July2023_track_only_AND_CMS_Golden.json")))


def getDatasets(dataVersion):
    if len(dataVersion) == 0:
        alldatasets = []
        for key in datasets.keys():
            alldatasets.extend(datasets[key])
        return alldatasets
    if not dataVersion in datasets.keys():
        print("Unknown dataVersion",dataVersion,", dataVersions available",datasets.keys())
        sys.exit()
    return datasets[dataVersion]
