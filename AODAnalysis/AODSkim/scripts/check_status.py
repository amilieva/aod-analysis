import os
import subprocess

def check_crab(current_dir):
    for subdir in os.listdir(current_dir):
        print(f"Processing {subdir}")
        try:
            subprocess.run(["crab", "status", subdir], check=True)
        except subprocess.CalledProcessError as e:
                print(f"Error while running crab status for {subdir}: {e}")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Check the status of crab jobs")
    parser.add_argument("directory", type=str, help="Crab directories")
    args = parser.parse_args()

    # root_dir = 'test/muticrab_xxx'

    check_crab(args.directory)
