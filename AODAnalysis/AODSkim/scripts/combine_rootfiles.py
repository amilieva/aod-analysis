#!/usr/bin/env python3

import os
import shutil
import subprocess
import ROOT

def combine_root_files(root_directory):
    for subdir, _, _ in os.walk(root_directory):
        results_dir = os.path.join(subdir, 'results')
        combined_output_file = os.path.join(results_dir, f'{os.path.basename(subdir)}_combined_output.root')

        # Check if the combined output file exists in the results directory
        if os.path.exists(results_dir):
            if not os.path.exists(combined_output_file):
                print(f"Combined output file does not exist in {results_dir}, running 'crab getoutput' in {subdir}")
                subprocess.run(["crab", "getoutput", "-d", subdir], check=True)

                if not os.listdir(results_dir):
                    print(f"Directory {subdir} is empty after 'crab getoutput', moving to next directory.")
                    continue

                print(f"Combining root files in {results_dir}")

                # Create a TChain and add root files to it
                chain = ROOT.TChain("ggll_aod/ntp1")
                root_files = [os.path.join(results_dir, filename) for filename in os.listdir(results_dir) if filename.startswith('output_') and filename.endswith('.root')]
                print(f"Adding root files to TChain: {root_files}")
                for root_file in root_files:
                    chain.Add(root_file)

                output_file_path = os.path.join(results_dir, f'{os.path.basename(subdir)}_combined_output.root')
                output_file = ROOT.TFile(output_file_path, 'recreate')

                # Copy the contents of the TChain to the output file
                chain.Merge(output_file_path)

                output_file.Close()
            else:
                print(f"Combined output file already exists in {results_dir}, moving to next directory.")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Combine root files in subdirectories named 'results'.")
    parser.add_argument("directory", type=str, help="Root directory containing subdirectories with root files.")
    args = parser.parse_args()

    ROOT.gROOT.SetBatch(True)  # Suppress graphical output from ROOT

    combine_root_files(args.directory)
