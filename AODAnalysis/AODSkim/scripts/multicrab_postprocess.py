#!/usr/bin/env python3

import os,sys,re
import subprocess
import json

import ROOT

from optparse import OptionParser
#from collections import OrderedDict

#from CRABClient.UserUtilities import setConsoleLogLevel
#from CRABClient.UserUtilities import getUsernameFromCRIC
#from CRABClient.ClientUtilities import LOGLEVEL_MUTE
#from CRABClient.UserUtilities import getConsoleLogLevel
from CRABAPI.RawCommand import crabCommand

from multicrabGetFromMadhatter import proxy,getMulticrabDirName,getCrabdirs,GetIncludeExcludeDatasets,getSEPath

USER = os.environ['USER']
STORAGE_ELEMENT_PATH = "https://hip-cms-se.csc.fi:2880/store/user/%s/CRAB3_TransferData"%(USER)
#STORAGE_ELEMENT_PATH = "gsiftp://madhatter.csc.fi/pnfs/csc.fi/data/cms//store/user/%s/CRAB3_TransferData/"%(USER)
#STORAGE_ELEMENT_PATH = "gsiftp://madhatter.csc.fi/pnfs/csc.fi/data/cms/store/group/local/Higgs/CRAB3_TransferData/"

USEARC = True

if USEARC:
    GRIDCOPY  = "arccp"
    GRIDLS    = "arcls"
    GRIDPROXY = "arcproxy --info"
else:
    GRIDCOPY  = "srmcp"
    GRIDLS    = "srmls"
    GRIDPROXY = "grid-proxy-info"

#from multicrab import execute
sumlumi = 0
lumidata = {}

def usage():
    print
    print("### Usage:   ",os.path.basename(sys.argv[0])," <multicrab dir|crab dir>")
    print
    sys.exit()

def execute(cmd):
    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    f = p.stdout
    ret=[]
    for line in f:
        line = line.decode('utf-8')
        ret.append(line.replace("\n", ""))
    f.close()
    return ret

def create_filelist(multicrabdir,taskdir,opts):

    from multicrabGetFromMadhatter import getSEPath,findRootFiles,restart_line,retrieve

    storagePath = getSEPath(multicrabdir)

    retrievePaths = {}

    if os.path.exists(os.path.join(multicrabdir,taskdir,"results","files_%s.json"%taskdir)):
        print("    - files list already created, skipping")
        return

    sys.stdout.write("    Scanning files at the SE: %s\n"%taskdir)
    gsipath = os.path.join(storagePath,multicrabdir)
    # print(gsipath)

    cands = execute("%s %s"%(GRIDLS,gsipath))
    for ddir in cands:
        cands2 = execute("%s %s"%(GRIDLS,os.path.join(gsipath,ddir)))
        for edir in cands2:
            if "crab_"+taskdir.strip("/") == edir.strip("/"):
                retrievePaths[taskdir] = os.path.join(gsipath,ddir,edir)
    if taskdir in retrievePaths.keys():
         retrieve(retrievePaths[taskdir],os.path.join(multicrabdir,taskdir,"results"),opts)
    else:
        print("\033[35m%s not retrieved\033[0m"%taskdir)
    
def main(opts, args):
    if len(sys.argv) < 2:
        usage()

    proxy()

    pIN = []
    if len(sys.argv) == 1:
        pIN.append(os.getcwd())
    else:
        pIN.extend(args)

    opts.includeTasks = opts.includeTasks.split(',')
    opts.excludeTasks = opts.excludeTasks.split(',')
    if 'None' in opts.includeTasks:
        opts.includeTasks = opts.includeTasks.remove('None')
    if 'None' in opts.excludeTasks:
        opts.excludeTasks = opts.excludeTasks.remove('None')

    basedir,multicrab = getMulticrabDirName(pIN[0])
    crabdirs  = getCrabdirs(os.path.join(basedir,multicrab),pIN)
    taskdirs = GetIncludeExcludeDatasets(crabdirs,opts)

    storagePath = getSEPath(os.path.join(basedir,multicrab))

    for taskdir in taskdirs:
        print("\033[34m\nProcessing taskdir %s\033[0m"%taskdir)
        #crab_report(taskdir) # create processedLumis.json
        create_filelist(multicrab,taskdir,opts)


if __name__=="__main__":
    parser = OptionParser(usage="Usage: %prog [options]")

    parser.add_option("--copy", dest="copy", default=False, action="store_true",
                      help="Copy root files from T2 to local disk [defaut: False]")
    parser.add_option("-i", "--includeTasks", dest="includeTasks", default="None", type="string",
                      help="Only perform action for this dataset(s) [default: \"\"]")

    parser.add_option("-e", "--excludeTasks", dest="excludeTasks", default="None", type="string",
                      help="Exclude this dataset(s) from action [default: \"\"]")
    parser.add_option("--overwrite", dest="overwrite", default=False, action="store_true",
                      help="Overwrite the pileup distributions [default: False")

    (opts, args) = parser.parse_args()
    main(opts, args)
