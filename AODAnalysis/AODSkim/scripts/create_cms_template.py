import ROOT

def cms_template(file_path, luminosity, energy, additional_text, cms_position, output_file):
    canvas = ROOT.TCanvas("canvas", "canvas", 800, 600)

    file = ROOT.TFile(file_path, "READ")
    if file.IsOpen():
        print(f"Opened ROOT file: {file_path}")
        for key in file.GetListOfKeys():
            obj = key.ReadObj()
            if isinstance(obj, ROOT.TH1) or isinstance(obj, ROOT.TH2):
                obj.Draw()

                file_name = file_path.split("/")[-1].split(".")[0]

                cms_text = ROOT.TLatex()
                cms_text.SetTextAlign(11)
                cms_text.SetTextFont(43)
                cms_text.SetTextSize(0.75 * canvas.GetTopMargin())
                cms_text.SetNDC()

                if cms_position == "top-left":
                    cms_text.DrawLatex(0.12, 0.88, "CMS")
                elif cms_position == "top-right":
                    cms_text.DrawLatex(0.88, 0.88, "CMS")
                else:
                    cms_text.DrawLatex(0.5, 0.88, "CMS")

                if additional_text:
                    additional_text_obj = ROOT.TLatex()
                    additional_text_obj.SetTextAlign(11)
                    additional_text_obj.SetTextFont(43)
                    additional_text_obj.SetTextSize(0.5 * canvas.GetTopMargin())
                    additional_text_obj.SetNDC()
                    additional_text_obj.DrawLatex(0.12, 0.82, additional_text)

                luminosity_text = ROOT.TLatex()
                luminosity_text.SetTextAlign(31)
                luminosity_text.SetTextFont(43)
                luminosity_text.SetTextSize(0.5 * canvas.GetTopMargin())
                luminosity_text.SetNDC()
                luminosity_text.DrawLatex(0.88, 0.92, f"{luminosity} ({energy})")

                output_file_name = f"{output_file}_{file_name}_{obj.GetName()}.png"
                canvas.Update()

                print(f"Saving plot to {output_file_name}")
                canvas.SaveAs(output_file_name)

    else:
        print(f"Failed to open ROOT file: {file_path}")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Creates the plots from given ROOT file")
    parser.add_argument("ROOTfile", type=str, help="Path to the actual ROOT file")
    args = parser.parse_args()

    ROOT.gROOT.SetBatch(True)  # Suppress graphical output from ROOT

    print(f"Processing ROOT file: {args.ROOTfile}")
    cms_template(args.ROOTfile, "20.1 fb^{-1}", "13 TeV", additional_text="Preliminary", cms_position="top-right", output_file="output")
