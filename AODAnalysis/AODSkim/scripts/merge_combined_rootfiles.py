#!/usr/bin/env python3

import os
import ROOT

def set_max_tree_size():
    ROOT.TTree.SetMaxTreeSize(1000000000000)  # 1 TB

def merge_root_files(root_directory):
    chain = ROOT.TChain("ntp1")

    for subdir, _, _ in os.walk(root_directory):
        results_dir = os.path.join(subdir, 'results')
        if os.path.exists(results_dir):
            combined_output_files = [f for f in os.listdir(results_dir) if f.endswith('_combined_output.root')]
            if combined_output_files:
                print(f"Merging root files in {results_dir}")
                for root_file in combined_output_files:
                    chain.Add(os.path.join(results_dir, root_file))

    if chain.GetEntries() > 0:
        output_file_path = os.path.join(root_directory, 'merged_output_test.root')
        output_file = ROOT.TFile(output_file_path, 'recreate')

        chain.Merge(output_file_path)
        output_file.Write()
        output_file.Close()
        print(f"All root files merged into a single file saved as {output_file_path}")
    else:
        print("No files with '_combined_output.root' found in any 'results' directory")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Merge all root files found within 'results' directories.")
    parser.add_argument("directory", type=str, help="Root directory containing subdirectories with root files.")
    args = parser.parse_args()

    ROOT.gROOT.SetBatch(True)  # Suppress graphical output from ROOT
    set_max_tree_size()  # Set the maximum tree size


    print(f"Checking {args.directory} for root files to merge...\n")


    # if not ("Run2023C" or "Run2023D") in args.directory:
    #     print(f"test - {args.directory} does not contain the expected string *_Run2023C_* or *_Run2023D_*")
    # else:
        # print(f"test - {args.directory}")
    merge_root_files(args.directory)
