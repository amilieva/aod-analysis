#!/usr/bin/env python3

import os
import subprocess
import ROOT

# multicrab copy

    #print(f"Running crab getoutput in {subdir}")
    #try:
    #subprocess.run(["crab", "getoutput", "-d", subdir], check=True)
    #except subprocess.CalledProcessError as e:
    #print(f"Error while running crab getoutput for {subdir}: {e}")

# arc copy

# data_muon0 = ['240325_103642/', '240325_103704/', '240325_103713/', '240325_103725/',
#                   '240325_103742/', '240325_103841/', '240325_103851/', '240325_111046/',
#                   '240325_111104/', '240325_111112/', '240325_111120/', '240325_111127/',
#                   '240325_111203/', '240325_111209/']

# data_muon1 = ['240325_103652/', '240325_103753/', '240325_103804/', '240325_103816/',
#                   '240325_103827/', '240325_103900/', '240325_103911/', '240325_111135/',
#                   '240325_111142/', '240325_111149/', '240325_111155/', '240325_111216/',
#                   '240325_111223/']

def merge_files(root_dir):
    
    if 'T1135' in root_dir:
        data_muon0 = ['240325_103642/', '240325_103704/', '240325_103713/', '240325_103725/',
                  '240325_103742/', '240325_103841/', '240325_103851/']

        data_muon1 = ['240325_103652/', '240325_103753/', '240325_103804/', '240325_103816/',
                  '240325_103827/', '240325_103900/', '240325_103911/']
    elif 'T1210' in root_dir:
        data_muon0 = ['240325_111046/', '240325_111104/', '240325_111112/', '240325_111120/', 
                  '240325_111127/', '240325_111203/', '240325_111209/']

        data_muon1 = ['240325_111135/', '240325_111142/', '240325_111149/', '240325_111155/', 
                  '240325_111216/', '240325_111223/']


    data = sorted(data_muon0 + data_muon1)

    for subdir, data_item in zip(sorted(os.listdir(root_dir)), data):
        subdir_path = os.path.join(root_dir, subdir)
        results_dir = os.path.join(subdir_path, 'results/')

        if os.path.exists(results_dir):
            # arc copy
            path = 'https://hip-cms-se.csc.fi:2880/store/user/amilieva/TEST/'

            if 'Muon0' in subdir:
                path += 'Muon0/AODAnalysis/'
            elif 'Muon1' in subdir:
                path += 'Muon1/AODAnalysis/'

            eopath = '0000/'

            path_i = path + data_item + eopath

            # copy all files from current directory
            # example: arccp -r https://hip-cms-se.csc.fi:2880/store/user/amilieva/TEST/Muon0/AODAnalysis/240325_111209/0000/ test_arccp/
            # -r for recursive copy
            
            print(f"\nCopying files from {path_i} to {results_dir}")

            try:
                subprocess.run(["arccp", "-r", path_i, results_dir], check=True)
            except subprocess.CalledProcessError as e:
                print(f"Error while copying files from {path_i} to {results_dir}: {e}")

            # merge files using hadd
            output_file = os.path.join(subdir_path, f'{subdir}_merged.root')
            files_to_merge = [os.path.join(results_dir, file) for file in os.listdir(results_dir) if file.startswith('output_') and file.endswith('.root')]
            
            print("\n")
            
            print("Running hadd")
            try:
                if files_to_merge:
                    subprocess.run(["hadd", output_file] + files_to_merge, check=True)
                    print(f"Merged files in {results_dir} into {output_file}")
                else:
                    print(f"No root files found in {results_dir}")
            except subprocess.CalledProcessError as e:
                print(f"Error while running hadd for {subdir}: {e}")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Takes root files from MadHatter and combine them with hadd command")
    parser.add_argument("directory", type=str, help="Directory containing subdirectories with root files.")
    args = parser.parse_args()

    ROOT.gROOT.SetBatch(True)  # Suppress graphical output from ROOT

    merge_files(args.directory)
